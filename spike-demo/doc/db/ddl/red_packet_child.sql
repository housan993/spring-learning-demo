/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3308
 Source Schema         : spike_db

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 11/07/2019 13:28:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for red_packet_child
-- ----------------------------
DROP TABLE IF EXISTS `red_packet_child`;
CREATE TABLE `red_packet_child`  (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `red_packet_id` int(64) NULL DEFAULT NULL COMMENT '红包id',
  `money` decimal(10, 0) NULL DEFAULT NULL COMMENT '红包金额',
  `deadline` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '红包状态',
  `user_id` int(64) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nk_red_packet_id`(`red_packet_id`) USING BTREE,
  INDEX `nk_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '红包拆分表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
