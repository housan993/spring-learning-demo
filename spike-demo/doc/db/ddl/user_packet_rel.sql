/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3308
 Source Schema         : spike_db

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 11/07/2019 13:28:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_packet_rel
-- ----------------------------
DROP TABLE IF EXISTS `user_packet_rel`;
CREATE TABLE `user_packet_rel`  (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `red_packet_id` int(64) NULL DEFAULT NULL COMMENT '红包id',
  `red_packet_child_id` int(64) NULL DEFAULT NULL COMMENT '红包拆分id',
  `user_id` int(64) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_rc_id`(`red_packet_id`, `red_packet_child_id`) USING BTREE,
  INDEX `nk_red_packet_id`(`red_packet_id`) USING BTREE,
  INDEX `nk_child_id`(`red_packet_child_id`) USING BTREE,
  INDEX `nk_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户红包关系表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
