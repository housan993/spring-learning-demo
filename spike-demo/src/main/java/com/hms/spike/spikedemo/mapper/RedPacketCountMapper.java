package com.hms.spike.spikedemo.mapper;

import com.hms.spike.spikedemo.entity.RedPacketCount;
import tk.mybatis.mapper.common.Mapper;

public interface RedPacketCountMapper extends Mapper<RedPacketCount> {

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}