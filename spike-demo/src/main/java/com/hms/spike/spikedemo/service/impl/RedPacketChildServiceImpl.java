package com.hms.spike.spikedemo.service.impl;


import com.google.common.collect.Lists;
import com.hms.spike.spikedemo.entity.RedPacket;
import com.hms.spike.spikedemo.entity.RedPacketChild;
import com.hms.spike.spikedemo.mapper.RedPacketChildMapper;
import com.hms.spike.spikedemo.service.RedPacketChildService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author hms
 * @date 2019/1/3 14:33
 */
@Service
public class RedPacketChildServiceImpl implements RedPacketChildService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedPacketChildMapper redPacketChildMapper;

    @Override
    public RedPacketChild selectById(Integer id) {
        return redPacketChildMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取红包金额 二倍均值法
     *
     * @param totalMoney 红包总金额 分
     * @param num        个数
     * @return 返回金额 分
     */
    public static BigDecimal getRandomMoney(double totalMoney, int num) {
        if (num == 1) {
            return BigDecimal.valueOf(totalMoney);
        }
        Random r = new Random();
        double min = 0.01;
        double max = totalMoney / num * 2;
        double money = r.nextDouble() * max;
        money = money <= min ? min : money;
        money = Math.floor(money * 100) / 100;
        return BigDecimal.valueOf(money);
    }

    @Override
    public void sendPacketChild(RedPacket redPacket) {
        int count = redPacket.getCount();
        int counter = redPacket.getCounter();
        int moneyTotal = redPacket.getMoney();

        List<RedPacketChild> redPacketChildList = Lists.newArrayList();
        for (int i = 0; i < count; i++) {
            RedPacketChild redPacketChild = new RedPacketChild();
            redPacketChild.setRedPacketId(redPacket.getId());

            double moneyTotalDouble = Double.parseDouble(String.valueOf(moneyTotal)) / 100;
            BigDecimal randomMoney =
                    getRandomMoney(moneyTotalDouble, counter);
            BigDecimal multiply = randomMoney.multiply(BigDecimal.valueOf(100));
            int money = multiply.intValue();
            redPacketChild.setMoney(money);
            redPacketChild.setStatus("1");
            counter--;
            moneyTotal -= money;
            redPacketChildList.add(redPacketChild);
        }

        log.info("redPacketChildList={}", redPacketChildList);

        for (RedPacketChild redPacketChild : redPacketChildList) {
            redPacketChildMapper.insert(redPacketChild);
        }
    }

    @Override
    public void updatePacketChild(Integer id, Integer userId) {
        RedPacketChild redPacketChild = new RedPacketChild();
        redPacketChild.setId(id);
        redPacketChild.setStatus("0");
        redPacketChild.setUserId(userId);
        redPacketChildMapper.updateByPrimaryKeySelective(redPacketChild);
    }

    @Override
    public List<RedPacketChild> selectByRedPacketId(Integer redPacketId) {
        return redPacketChildMapper.selectByRedPacketId(redPacketId);
    }

    /**
     * 线段切割法
     *
     * @param amount 红包金额 元
     * @param friend 红包数量
     */
    private static int[] lineCutting(int amount, int friend) {
        int[] redPacket = new int[friend];
        //将金额数按照一分钱来等分100*amount分
        int seed = 100 * amount;
        int length = 0;
        //在0~seed中生成lineCount个随机不重复的数字
        int lineCount = friend - 1;

        //利用SecureRandom生成随机数
        SecureRandom secureRandom = null;
        //保证不重复且升序排序
        SortedSet<Integer> set = new TreeSet<>();
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        while (length < lineCount) {
            assert secureRandom != null;
            int num = secureRandom.nextInt(seed);
            if (num == 0) {
                num = 1;
            }
            set.add(num);
            length = set.size();
        }

        int preNum = 0;
        int currentNum = 0;
        int calc = 0;
        for (int i = 0; i < set.toArray().length + 1; i++) {
            if (i < set.toArray().length) {
                currentNum = (int) set.toArray()[i];
            } else {
                currentNum = seed;
            }

            System.out.print("第" + (i + 1) + "个红包 ");
            BigDecimal tmpMoney = new BigDecimal((currentNum - preNum)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
            System.out.print("红包金额=" + tmpMoney + "元 ");
            redPacket[i] = (currentNum - preNum);
            calc += (currentNum - preNum);
            System.out.println("已发:" + new BigDecimal(calc).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP) + "元");
            preNum = currentNum;
        }

        return redPacket;
    }

    /**
     * 保底平均随机法
     * 100分钱10人分
     * 保底1分钱 剩余钱/剩余人数=余额平均钱
     * 随机0-0.99余额平均钱 + 保底1分钱 = 最终获得钱
     * 循环计算 最后一人获得剩余全部钱 前面的人获得随机钱 + 保底1分钱
     *
     * @param totalMoney  红包金额 元
     * @param totalPeople 红包数量
     */
    private static double[] doubleAverage(double totalMoney, int totalPeople) {
        double[] redPacket = new double[totalPeople];
        // 每个人最少能收到0.01元
        double minMoney = 0.01;

        for (int i = 0; i < totalPeople - 1; i++) {
            int j = i + 1;
            double safeMoney = (totalMoney - (totalPeople - j) * minMoney) / (totalPeople - j);
            double tmpMoney = (Math.random() * (safeMoney * 100 - minMoney * 100) + minMoney * 100) / 100;
            tmpMoney = Math.round(tmpMoney * 100) / 100.0;
            totalMoney -= tmpMoney;
            System.out.format("第 %d 个红包： %.2f 元，剩下： %.2f 元\n", j, tmpMoney, totalMoney);
            redPacket[i] = tmpMoney;
        }
        System.out.format("第 %d 个红包： %.2f 元，剩下： 0 元\n", totalPeople, totalMoney);
        totalMoney = Math.round(totalMoney * 100) / 100.0;
        redPacket[totalPeople - 1] = totalMoney;

        return redPacket;
    }

    private static void lineCuttingPrint(int money, int count){
        int[] lineCutting = new int[count];

        long startTime = System.currentTimeMillis();

        lineCutting = lineCutting(money, count);

        long endTime = System.currentTimeMillis();

        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");

        int allInt = 0;
        for (int i = 0; i < count; i++) {
            System.out.println(new BigDecimal(lineCutting[i]).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));
            allInt += lineCutting[i];
        }
        System.out.println(new BigDecimal(allInt).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));
    }

    private static void doubleAveragePrint(int money, int count){
        double[] doubleAverage = new double[count];

        long startTime = System.currentTimeMillis();

        doubleAverage = doubleAverage(money, count);

        long endTime = System.currentTimeMillis();

        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");

        double all = 0;
        for (int i = 0; i < count; i++) {
            System.out.println(doubleAverage[i]);
            all += doubleAverage[i];
        }
        System.out.println(all);

    }

    public static void main(String[] args) {
        int money = 100;
        int count = 8;

        lineCuttingPrint(money, count);

        doubleAveragePrint(money, count);
    }
}
