package com.hms.spike.spikedemo.exception;

/**
 * @author hms
 */
public class BizException extends Exception {

    private String code;

    public BizException(String message) {
        super(message);
    }

    public BizException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
