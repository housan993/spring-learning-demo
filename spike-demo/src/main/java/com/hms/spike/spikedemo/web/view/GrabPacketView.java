package com.hms.spike.spikedemo.web.view;

import com.hms.spike.spikedemo.base.BaseEntity;

import javax.persistence.Column;
import java.util.Date;

/**
 * @author hms
 * @date 2019/1/7 11:21
 */
public class GrabPacketView extends BaseEntity {
    private Integer redPacketId;

    private Integer money;

    private String status;

    private String msg;

    private Integer userId;

    private String overStatus;

    public Integer getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(Integer redPacketId) {
        this.redPacketId = redPacketId;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOverStatus() {
        return overStatus;
    }

    public void setOverStatus(String overStatus) {
        this.overStatus = overStatus;
    }
}
