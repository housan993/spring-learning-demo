package com.hms.spike.spikedemo.exception;


/**
 * @author hms
 */
public class BizRuntimeException extends RuntimeException {

    private String code;

    public BizRuntimeException(String message) {
        super(message);
    }

    public BizRuntimeException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
