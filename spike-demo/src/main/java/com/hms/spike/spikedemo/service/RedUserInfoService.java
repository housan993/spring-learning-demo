package com.hms.spike.spikedemo.service;


import com.hms.spike.spikedemo.entity.RedUserInfo;

/**
 *
 *
 * @author hms
 * @date 2019/1/3 14:32
 */
public interface RedUserInfoService {

    /**
     * 插入用户信息
     * @param redUserInfo 用户信息
     */
    void insert(RedUserInfo redUserInfo);


}
