package com.hms.spike.spikedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpikeDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpikeDemoApplication.class, args);
    }

}

