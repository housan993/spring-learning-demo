package com.hms.spike.spikedemo.service;


import com.hms.spike.spikedemo.entity.RedPacket;
import com.hms.spike.spikedemo.entity.RedPacketChild;

import java.util.List;

/**
 *
 *
 * @author hms
 * @date 2019/1/3 14:32
 */
public interface RedPacketChildService {

    /**
     * 生成红包
     * @param redPacket 红包类型
     */
    void sendPacketChild(RedPacket redPacket);

    /**
     * 更新红包用户id
     * @param id
     * @param userId
     */
    void updatePacketChild(Integer id, Integer userId);

    List<RedPacketChild> selectByRedPacketId(Integer redId);

    RedPacketChild selectById(Integer id);

}
