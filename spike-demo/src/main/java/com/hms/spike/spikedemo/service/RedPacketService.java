package com.hms.spike.spikedemo.service;


import com.hms.spike.spikedemo.entity.RedPacket;

/**
 *
 *
 * @author hms
 * @date 2019/1/3 14:32
 */
public interface RedPacketService {

    /**
     * 发红包
     * @param packetName 红包名称
     * @param money 金额
     * @param count 数量
     */
    Integer sendPacket(String packetName, Integer money, Integer count);

    RedPacket selectForUpdate(Integer redId);

    RedPacket selectById(Integer redId);

    void updateCounter(Integer redId);

}
