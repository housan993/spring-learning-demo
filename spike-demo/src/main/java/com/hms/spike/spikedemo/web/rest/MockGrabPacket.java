package com.hms.spike.spikedemo.web.rest;

import com.hms.spike.spikedemo.service.impl.SpikeLockServiceImpl;
import com.hms.spike.spikedemo.utils.SpringContextHolderUtils;
import com.hms.spike.spikedemo.web.criteria.GrabPacketCriteria;

import java.util.concurrent.CountDownLatch;

/**
 * @author hms
 */
public class MockGrabPacket extends Thread {
    private GrabPacketCriteria packetCriteria;
    private CountDownLatch latch;

    private SpikeLockServiceImpl spikeLockServiceImpl;


    public MockGrabPacket(GrabPacketCriteria packetCriteria, CountDownLatch latch) {
        super();
        this.packetCriteria = packetCriteria;
        this.latch = latch;

        if (null == this.spikeLockServiceImpl) {
            this.spikeLockServiceImpl = SpringContextHolderUtils.getBean(SpikeLockServiceImpl.class);
        }
    }

    @Override
    public void run() {
        try {
            latch.await(); //一直阻塞当前线程，直到计时器的值为0
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        grabPacket();//发送post 请求

    }

    private void grabPacket() {
        spikeLockServiceImpl.grabPacket(packetCriteria);
    }
}
