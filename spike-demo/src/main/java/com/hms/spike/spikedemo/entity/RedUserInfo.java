package com.hms.spike.spikedemo.entity;

import com.hms.spike.spikedemo.base.BaseEntity;
import javax.persistence.*;

@Table(name = "red_user_info")
public class RedUserInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_name")
    private String userName;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}