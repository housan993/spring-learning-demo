package com.hms.spike.spikedemo.entity;

import com.hms.spike.spikedemo.base.BaseEntity;
import javax.persistence.*;

@Table(name = "red_packet")
public class RedPacket extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Integer money;

    private Integer count;

    /**
     * 计数
     */
    private Integer counter;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return money
     */
    public Integer getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(Integer money) {
        this.money = money;
    }

    /**
     * @return count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 获取计数
     *
     * @return counter - 计数
     */
    public Integer getCounter() {
        return counter;
    }

    /**
     * 设置计数
     *
     * @param counter 计数
     */
    public void setCounter(Integer counter) {
        this.counter = counter;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}