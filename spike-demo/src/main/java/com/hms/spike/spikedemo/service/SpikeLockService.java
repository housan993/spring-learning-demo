package com.hms.spike.spikedemo.service;


import com.hms.spike.spikedemo.web.criteria.GetLockRequest;
import com.hms.spike.spikedemo.web.criteria.GrabPacketCriteria;
import com.hms.spike.spikedemo.web.view.GetLockResponse;
import com.hms.spike.spikedemo.web.view.GrabPacketView;

/**
 *
 *
 * @author hms
 * @date 2019/1/3 14:32
 */
public interface SpikeLockService {


    boolean lockSetTime(String key, Integer expireTime);


    GetLockResponse getLock(GetLockRequest request);

    /**
     * 抢红包
     * @param grabPacketCriteria
     * @return
     */
    GrabPacketView grabPacket(GrabPacketCriteria grabPacketCriteria);

    void mockGrabPacket(GrabPacketCriteria grabPacketCriteria);

    void mysqlSiege(int num);

    void selectOne();
}
