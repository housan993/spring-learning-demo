package com.hms.spike.spikedemo.config;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * @author hms
 * @date 2018/11/5 14:51
 */
@Configuration
public class ExecutorConfig {

    private static final Logger logger = LoggerFactory.getLogger(ExecutorConfig.class);

    @Value("${executorService.poolSize}")
    private int poolSize = 10;

    @Bean
    public ExecutorService executorService() {
        logger.info("自定义线程池poolSize=" + poolSize);
        return new ScheduledThreadPoolExecutor(poolSize,
                new BasicThreadFactory.Builder().namingPattern("example-schedule-pool-%d")
                        .daemon(true).build());
    }

}
