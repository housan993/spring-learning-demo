package com.hms.spike.spikedemo.service.impl;


import com.google.common.base.Strings;
import com.hms.spike.spikedemo.config.RedisHelper;
import com.hms.spike.spikedemo.entity.RedPacket;
import com.hms.spike.spikedemo.entity.RedPacketChild;
import com.hms.spike.spikedemo.entity.SpikeLock;
import com.hms.spike.spikedemo.entity.UserPacketRel;
import com.hms.spike.spikedemo.mapper.SpikeLockMapper;
import com.hms.spike.spikedemo.mapper.UserPacketRelMapper;
import com.hms.spike.spikedemo.service.RedPacketChildService;
import com.hms.spike.spikedemo.service.RedPacketService;
import com.hms.spike.spikedemo.service.SpikeLockService;
import com.hms.spike.spikedemo.web.criteria.GetLockRequest;
import com.hms.spike.spikedemo.web.criteria.GrabPacketCriteria;
import com.hms.spike.spikedemo.web.rest.MockGrabPacket;
import com.hms.spike.spikedemo.web.rest.MysqlSiege;
import com.hms.spike.spikedemo.web.view.GetLockResponse;
import com.hms.spike.spikedemo.web.view.GrabPacketView;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.CRC32;

/**
 * @author hms
 * @date 2019/1/3 14:33
 */
@Service
public class SpikeLockServiceImpl implements SpikeLockService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private Environment environment;

    @Resource
    private SpikeLockMapper spikeLockMapper;

    @Resource
    private RedPacketService redPacketService;

    @Resource
    private RedPacketChildService redPacketChildService;

    @Resource
    private RedisHelper redisHelper;

    @Resource
    private UserPacketRelMapper userPacketRelMapper;

    @Resource
    private SpikeLockServiceImpl spikeLockServiceImpl;

    private static AtomicInteger ai = new AtomicInteger();

    private static AtomicInteger mysqlNum = new AtomicInteger(0);

    private static AtomicLong start = new AtomicLong();

    private static int selectNum = 0;

    @Override
    public boolean lockSetTime(String key, Integer expireTime) {
        boolean keyFlag = redisHelper.getDistributedLockSetTime(key, expireTime);
        if (keyFlag) {
            log.info("redis抢到锁了");
        } else {
            log.info("redis没抢到锁");
        }
        return keyFlag;
    }

    /**
     * 初始化记录,如果有记录update,如果没有记录insert
     */
    private SpikeLock initLockRecord(String key) {
        // 查询记录是否存在
        SpikeLock spikeLock = spikeLockMapper.selectBySpikeLockName(key);
        if (null == spikeLock) {
            // 记录不存在，创建

        }
        return spikeLock;
    }

    /**
     * 获取中央锁Key
     */
    private boolean getCenterLock(String key) {
        String prefix = "center_lock_";
        CRC32 crc32 = new CRC32();
        crc32.update(key.getBytes());
        crc32.getValue();

        Long hash = crc32.getValue();
        //取crc32中的最后两位值
        Integer len = hash.toString().length();
        String slot = hash.toString().substring(len - 2);

        String centerLockKey = prefix + slot;
        log.info("centerLockKey={}", centerLockKey);
        spikeLockMapper.selectForUpdate(centerLockKey);
        return true;
    }


    /**
     * 获取锁，代码片段
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public GetLockResponse getLock(GetLockRequest request) {
        // 检测参数
        if (StringUtils.isEmpty(request.getLockName())) {
            throw new RuntimeException("key不能为空");
        }

        // 兼容参数初始化
        request.setExpireTime(null == request.getExpireTime() ? 31536000 : request.getExpireTime());
        request.setDesc(Strings.isNullOrEmpty(request.getDesc()) ? "" : request.getDesc());
        long nowTime = System.currentTimeMillis();

        GetLockResponse response = new GetLockResponse();
        response.setLock(0);

        boolean b = lockSetTime(request.getLockName(), 30);
        if (b) {
            log.info("redis获取到锁了lockName={}", request.getLockName());
        } else {
            log.info("redis未获取到锁lockName={}", request.getLockName());
            return response;
        }

        // 获取中央锁，初始化记录
        getCenterLock(request.getLockName());
        SpikeLock spikeLock = initLockRecord(request.getLockName());

        ai.incrementAndGet();
        log.error("测试信息*************************************" + ai);
//        // 未释放锁或未过期，获取失败
//        if (spikeLock.getFstatus() == (byte) 1
//                && spikeLock.getFdeadline().getTime() > nowTime) {
//            return response;
//        }
//
//        // 获取锁
//        Date deadline = new Date(nowTime + request.getExpireTime() * 1000);
//
//        SpikeLock lockDemo = new SpikeLock();
//        lockDemo.setFindex(spikeLock.getFindex());
//        lockDemo.setFlockName(request.getLockName());
//        lockDemo.setFdeadline(deadline);
//        lockDemo.setFcount(0);
//        lockDemo.setFdesc(request.getDesc());
//        lockDemo.setFstatus((byte) 1);
//
//        lockDemo.setFmodifyTime(new Date());
//
//        int num = lockDemoMapper.updateByPrimaryKeySelective(lockDemo);
        response.setLock(1);

        log.error("测试信息1*************************************");
        response.setNum(ai.intValue());
        return response;
    }

    /**
     * mysql默认事物隔离级别是RR
     * spring默认事物隔离级别是数据库默认级别
     * 这里髙并发需要RC
     *
     * @param grabPacketCriteria
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED,
            isolation = Isolation.READ_COMMITTED)
    public GrabPacketView grabPacket(GrabPacketCriteria grabPacketCriteria) {
        Integer userId = grabPacketCriteria.getUserId();
        Integer redId = grabPacketCriteria.getRedId();
        // 检测参数
        if (null == userId) {
            throw new RuntimeException("用户id不能为空");
        }
        if (null == redId) {
            throw new RuntimeException("红包id不能为空");
        }

        String status = grabPacketCriteria.getStatus();
        if (!Strings.isNullOrEmpty(status)
                && "1".equals(status)) {
            GrabPacketView grabPacketView = new GrabPacketView();
            grabPacketView.setStatus("1");
            grabPacketView.setRedPacketId(redId);
            grabPacketView.setUserId(userId);

            UserPacketRel userPacketRel = userPacketRelMapper.selectByRedIdAndUserId(redId, userId);
            if (Objects.isNull(userPacketRel)) {
                log.error("用户[{}]红包[{}]信息不存在", userId, redId);
                grabPacketView.setMsg("没有红包");
                return grabPacketView;
            }
            RedPacketChild redPacketChild = redPacketChildService.selectById(userPacketRel.getRedPacketChildId());
            if (Objects.isNull(redPacketChild)) {
                log.error("用户[{}]红包[{}]详细信息[{}]不存在", userId, redId, redPacketChild);
                grabPacketView.setMsg("没有红包");
                return grabPacketView;
            }
            grabPacketView.setMoney(redPacketChild.getMoney());
            return grabPacketView;
        }

        String overStatus = grabPacketCriteria.getOverStatus();
        if (!Strings.isNullOrEmpty(overStatus)
                && "1".equals(overStatus)) {
            GrabPacketView grabPacketView = new GrabPacketView();
            grabPacketView.setStatus("1");
            grabPacketView.setRedPacketId(redId);
            grabPacketView.setUserId(userId);
            log.info("红包已抢完");
            grabPacketView.setMsg("红包已抢完");
            return grabPacketView;
        }
        RedPacket redPacket = redPacketService.selectById(redId);
        if (null == redPacket) {
            throw new RuntimeException("红包[" + redId + "]信息不存在");
        }

        String key = "red_packet|" + userId + "|" + redId;

        // 兼容参数初始化
        grabPacketCriteria.setExpireTime(
                null == grabPacketCriteria.getExpireTime() ? 30 : grabPacketCriteria.getExpireTime());
        grabPacketCriteria.setDesc(
                Strings.isNullOrEmpty(grabPacketCriteria.getDesc()) ? "" : grabPacketCriteria.getDesc());
        long nowTime = System.currentTimeMillis();

        GrabPacketView grabPacketView = new GrabPacketView();
        grabPacketView.setStatus("0");

        boolean b = lockSetTime(key, 30);
        if (b) {
            log.info("redis获取到锁了key={}", key);
        } else {
            log.info("redis未获取到锁key={}", key);
            grabPacketView.setMsg("正在抢,请稍候...");
            return grabPacketView;
        }

        String redKey = "red_packet_" + redId;
        // 获取中央锁，初始化记录
        getCenterLock(redKey);

        SpikeLock spikeLock = initLockRed(redKey, redPacket.getCount());

        log.info("redKey=" + redKey);
        if (spikeLock.getSpikeCount() == 0) {
            log.info("红包已抢完");
            grabPacketView.setMsg("红包已抢完");
            grabPacketView.setOverStatus("1");
            return grabPacketView;
        }
        RedPacketChild redPacketChild = spikeLockServiceImpl.save(redId, userId);
        if (null == redPacketChild) {
            log.info("红包已抢完");
            grabPacketView.setMsg("红包已抢完");
            return grabPacketView;
        }
        log.info("用户[{}]抢到红包[{}]的金额[{}]", userId, redId, redPacketChild.getMoney());

        spikeLockMapper.updateCounter(spikeLock.getSpikeIndex());
        grabPacketView.setStatus("1");
        grabPacketView.setMsg("恭喜你，抢到了红包");
        grabPacketView.setMoney(redPacketChild.getMoney());
        log.info("抢到红包了,grabPacketView={}", grabPacketView);
        return grabPacketView;
    }


    /**
     * 初始化记录,如果有记录update,如果没有记录insert
     */
    private SpikeLock initLockRed(String key, Integer count) {
        // 查询记录是否存在
        SpikeLock spikeLock = spikeLockMapper.selectBySpikeLockName(key);
        if (null == spikeLock) {
            // 记录不存在，创建
            spikeLock = new SpikeLock();
            spikeLock.setSpikeLockName(key);
            spikeLock.setSpikeCount(count);
            spikeLock.setSpikeDeadline(new Date());
            spikeLock.setSpikeDesc("");
            spikeLock.setSpikeCreateTime(new Date());
            spikeLock.setSpikeModifyTime(new Date());
            spikeLock.setSpikeStatus((byte) 1);
            spikeLockMapper.insert(spikeLock);
        }
        return spikeLock;
    }

    @Transactional(rollbackFor = Exception.class)
    public RedPacketChild save(Integer redId, Integer userId) {

        redPacketService.updateCounter(redId);
//        插入用户抢到的红包id关系表

        List<RedPacketChild> redPacketChildren = redPacketChildService.selectByRedPacketId(redId);

        if (CollectionUtils.isEmpty(redPacketChildren)) {
            log.error("红包已经没了");
            return null;
        }
        RedPacketChild redPacketChild = redPacketChildren.get(0);
        Integer childId = redPacketChild.getId();

        redPacketChildService.updatePacketChild(childId, userId);

        UserPacketRel userPacketRel = userPacketRelMapper.selectByRedIdAndUserId(redId, userId);
        if (Objects.isNull(userPacketRel)) {
            userPacketRel = new UserPacketRel();
            userPacketRel.setUserId(userId);
            userPacketRel.setRedPacketId(redId);
            userPacketRel.setRedPacketChildId(childId);
            userPacketRelMapper.insert(userPacketRel);
        } else {
            throw new RuntimeException("你已抢过红包");
        }

        return redPacketChild;
    }

    /**
     * 初始化记录,如果有记录update,如果没有记录insert
     */
    private RedPacket lockRecord(String key, int redId) {
        RedPacket redPacket = redPacketService.selectForUpdate(redId);

        // 查询记录是否存在
        SpikeLock spikeLock = spikeLockMapper.selectBySpikeLockName(key);
        if (null == spikeLock) {
            // 记录不存在，创建

        }
        return redPacket;
    }

    @Override
    public void mockGrabPacket(GrabPacketCriteria grabPacketCriteria) {

        CountDownLatch latch = new CountDownLatch(1);

        for (int i = 0; i < 50; i++) {
            GrabPacketCriteria packetCriteria = new GrabPacketCriteria();
            packetCriteria.setRedId(grabPacketCriteria.getRedId());
            packetCriteria.setUserId(i + 1);

            MockGrabPacket mockGrabPacket = new MockGrabPacket(packetCriteria, latch);
            mockGrabPacket.start();
        }

        for (int i = 35; i < 75; i++) {
            GrabPacketCriteria packetCriteria = new GrabPacketCriteria();
            packetCriteria.setRedId(grabPacketCriteria.getRedId());
            packetCriteria.setUserId(i + 1);

            MockGrabPacket mockGrabPacket = new MockGrabPacket(packetCriteria, latch);
            mockGrabPacket.start();
        }

        for (int i = 0; i < 2; i++) {
            GrabPacketCriteria packetCriteria = new GrabPacketCriteria();
            packetCriteria.setRedId(grabPacketCriteria.getRedId());
            packetCriteria.setUserId(i + 1);

            MockGrabPacket mockGrabPacket = new MockGrabPacket(packetCriteria, latch);
            mockGrabPacket.start();
        }

        //计数器減一  所有线程释放 并发访问。
        latch.countDown();
    }

    @Override
    public void mysqlSiege(int num) {
        int maxNum = 10000;
        if (num < 0 || num >= maxNum) {
            log.error("线程数请设置在1~10000以内");
            return;
        }
        selectNum = num - 1;
        CountDownLatch latch = new CountDownLatch(1);
        for (int i = 0; i < num; i++) {
            MysqlSiege mysqlSiege = new MysqlSiege(latch);
            mysqlSiege.start();
        }
        // 计数器減一  所有线程释放 并发访问。
        latch.countDown();
    }

    @Override
    public void selectOne() {
        int andIncrement = mysqlNum.getAndIncrement();
        log.info("mysql查询{}", andIncrement);
        if (0 == andIncrement) {
            start.getAndSet(System.currentTimeMillis());
            log.error("start={}", start.get());
        }
        redPacketService.selectById(1);
        if (selectNum == andIncrement) {
            long end = System.currentTimeMillis();
            log.error("start-end={}", start.get());
            long total = end - start.getAndIncrement() - 1;
            float avg = (float) selectNum / (float) total;
            log.error("total={}", total);
            log.error("avg={}", avg * 1000);
            mysqlNum.set(0);
            start.set(0);
        }
    }
}
