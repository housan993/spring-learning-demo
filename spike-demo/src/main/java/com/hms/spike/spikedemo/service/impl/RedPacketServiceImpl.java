package com.hms.spike.spikedemo.service.impl;


import com.hms.spike.spikedemo.entity.RedPacket;
import com.hms.spike.spikedemo.mapper.RedPacketMapper;
import com.hms.spike.spikedemo.mapper.UserPacketRelMapper;
import com.hms.spike.spikedemo.service.RedPacketChildService;
import com.hms.spike.spikedemo.service.RedPacketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author hms
 * @date 2019/1/3 14:33
 */
@Service
public class RedPacketServiceImpl implements RedPacketService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedPacketMapper redPacketMapper;
    @Resource
    private RedPacketChildService redPacketChildService;

    @Override
    public Integer sendPacket(String packetName, Integer money, Integer count) {
        RedPacket redPacket = new RedPacket();
        redPacket.setName(packetName);
        redPacket.setMoney(money);
        redPacket.setCount(count);
        redPacket.setCounter(count);
        redPacketMapper.insert(redPacket);

        redPacketChildService.sendPacketChild(redPacket);
        return redPacket.getId();
    }

    @Override
    public RedPacket selectForUpdate(Integer redId) {
       return redPacketMapper.selectForUpdate(redId);
    }

    @Override
    public RedPacket selectById(Integer redId) {
        return redPacketMapper.selectByPrimaryKey(redId);
    }

    @Override
    public void updateCounter(Integer redId) {
        redPacketMapper.updateCounter(redId);
    }
}
