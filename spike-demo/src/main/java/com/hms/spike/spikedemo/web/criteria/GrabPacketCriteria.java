package com.hms.spike.spikedemo.web.criteria;

import com.hms.spike.spikedemo.base.BaseEntity;

/**
 * @author hms
 * @date 2019/1/7 11:24
 */
public class GrabPacketCriteria extends BaseEntity {
    private Integer userId;
    private Integer redId;
    private Long expireTime;
    private String desc;
    private String status;

    private String overStatus;

    public Integer getRedId() {
        return redId;
    }

    public void setRedId(Integer redId) {
        this.redId = redId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOverStatus() {
        return overStatus;
    }

    public void setOverStatus(String overStatus) {
        this.overStatus = overStatus;
    }
}
