package com.hms.spike.spikedemo.entity;

import com.hms.spike.spikedemo.base.BaseEntity;
import javax.persistence.*;

@Table(name = "red_packet_count")
public class RedPacketCount extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "red_packet_id")
    private Integer redPacketId;

    private Integer count;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return red_packet_id
     */
    public Integer getRedPacketId() {
        return redPacketId;
    }

    /**
     * @param redPacketId
     */
    public void setRedPacketId(Integer redPacketId) {
        this.redPacketId = redPacketId;
    }

    /**
     * @return count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}