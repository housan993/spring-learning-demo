package com.hms.spike.spikedemo.web.rest;

import com.hms.spike.spikedemo.service.impl.SpikeLockServiceImpl;
import com.hms.spike.spikedemo.utils.SpringContextHolderUtils;

import java.util.concurrent.CountDownLatch;

/**
 * @author hms
 */
public class MysqlSiege extends Thread {
    private CountDownLatch latch;

    private SpikeLockServiceImpl spikeLockServiceImpl;


    public MysqlSiege(CountDownLatch latch) {
        super();
        this.latch = latch;
        if (null == this.spikeLockServiceImpl) {
            this.spikeLockServiceImpl = SpringContextHolderUtils.getBean(SpikeLockServiceImpl.class);
        }
    }

    @Override
    public void run() {
        try {
            // 一直阻塞当前线程，直到计时器的值为0
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        selectOne();
    }

    private void selectOne() {
        spikeLockServiceImpl.selectOne();
    }
}
