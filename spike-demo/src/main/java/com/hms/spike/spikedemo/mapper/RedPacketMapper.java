package com.hms.spike.spikedemo.mapper;

import com.hms.spike.spikedemo.entity.RedPacket;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

public interface RedPacketMapper extends Mapper<RedPacket> {

	/*CUSTOM_CODE_START*/

    @Select("SELECT * from red_packet WHERE id = #{redId} for update ")
    RedPacket selectForUpdate(@Param("redId") Integer redId);

    @Update("update red_packet set counter = counter - 1 WHERE id = #{redId} and counter > 0 ")
    void updateCounter(@Param("redId") Integer redId);

	/*CUSTOM_CODE_END*/
}