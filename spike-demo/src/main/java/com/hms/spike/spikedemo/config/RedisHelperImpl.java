package com.hms.spike.spikedemo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author hms
 * @date 2019/1/3 16:19
 */
@Service
public class RedisHelperImpl<HK, T> implements RedisHelper<HK, T> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 在构造器中获取redisTemplate实例, key(not hashKey) 默认使用String类型
     */
    private RedisTemplate<String, T> redisTemplate;
    /**
     * 在构造器中通过redisTemplate的工厂方法实例化操作对象
     */
    private HashOperations<String, HK, T> hashOperations;
    private ListOperations<String, T> listOperations;
    private ZSetOperations<String, T> zSetOperations;
    private SetOperations<String, T> setOperations;
    private ValueOperations<String, T> valueOperations;

    /**
     * IDEA虽然报错,但是依然可以注入成功, 实例化操作对象后就可以直接调用方法操作Redis数据库
     *
     * @param redisTemplate
     */
    @Autowired
    public RedisHelperImpl(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
                                   RedisTemplate<String, T> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = redisTemplate.opsForHash();
        this.listOperations = redisTemplate.opsForList();
        this.zSetOperations = redisTemplate.opsForZSet();
        this.setOperations = redisTemplate.opsForSet();
        this.valueOperations = redisTemplate.opsForValue();
    }

    @Override
    public void hashPut(String key, HK hashKey, T domain) {
        hashOperations.put(key, hashKey, domain);
    }

    @Override
    public Map<HK, T> hashFindAll(String key) {
        return hashOperations.entries(key);
    }

    @Override
    public T hashGet(String key, HK hashKey) {
        return hashOperations.get(key, hashKey);
    }

    @Override
    public void hashRemove(String key, HK hashKey) {
        hashOperations.delete(key, hashKey);
    }

    @Override
    public Long listPush(String key, T domain) {
        return listOperations.rightPush(key, domain);
    }

    @Override
    public Long listUnshift(String key, T domain) {
        return listOperations.leftPush(key, domain);
    }

    @Override
    public List<T> listFindAll(String key) {
        if (!redisTemplate.hasKey(key)) {
            return null;
        }
        return listOperations.range(key, 0, listOperations.size(key));
    }

    @Override
    public T listLPop(String key) {
        return listOperations.leftPop(key);
    }

    @Override
    public void valuePut(String key, T domain) {
        valueOperations.set(key, domain);
    }

    @Override
    public T getValue(String key) {
        return valueOperations.get(key);
    }

    @Override
    public int setIfAbsent(String key, T value) {
        if (valueOperations.setIfAbsent(key, value)) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void remove(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public boolean expirse(String key, long timeout, TimeUnit timeUnit) {
        return redisTemplate.expire(key, timeout, timeUnit);
    }

    @Override
    public boolean getDistributedLockSetTime(String key, Integer expireTime) {
        try {
            // 移除已经失效的锁
            Long temp = (Long) getValue(key);
            Long currentTime = (new Date()).getTime();
            if (null != temp && temp < currentTime) {
                remove(key);
            }

            // 锁竞争
            Long nextTime = currentTime + Long.valueOf(expireTime) * 1000;
            int result = setIfAbsent(key, (T) nextTime);
            if (result == 1) {
                expirse(key, expireTime, TimeUnit.SECONDS);
                return true;
            }
        } catch (Exception e) {
            log.error("redis获取锁失败", e);
        }
        return false;
    }


}
