package com.hms.spike.spikedemo.utils;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <p>缓存工具类</p >
 *
 * @author Mitchell
 * @version 1.0
 * @date 2020/05/18 16:48
 */
@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
//@RequiredArgsConstructor(onConstructor_={@Autowired})
public class RedisCacheUtils {

    private final RedisLockerTemplate redisLockerTemplate;

    /**
     * 防止缓存击穿&穿透
     *
     * @param request
     * @param <T>
     * @return
     */
    public <T> T ifExpired4ReGet(CacheExpiredReGetRequest<T> request) {
        return ifExpired4ReGet(request.getGetCacheAction(),
                request.getExpiredAction(),
                request.getDataSourceAction(),
                request.getSetCacheAction(),
                request.getLockKey(),
                request.getEmpty(),
                request.getReturnEmpty(),
                request.getHystrixStrategy()
        );
    }

    /**
     * 防止缓存击穿&穿透
     *
     * @param getCacheAction   获取缓存数据的操作
     * @param expiredAction    判断该缓存数据是否失效的行为
     * @param dataSourceAction 从数据源获取数据的操作
     * @param setCacheAction   将数据缓存的行为
     * @param lockKey          锁key
     * @param empty            空值定义
     * @param returnEmpty      当获取的数据为empty时，返回returnEmpty
     * @param hystrixStrategy  保底策略
     * @param <T>
     * @return
     */
    private <T> T ifExpired4ReGet(Supplier<T> getCacheAction, Function<T, Boolean> expiredAction,
                                  Supplier<T> dataSourceAction, Consumer<T> setCacheAction, String lockKey,
                                  T empty, T returnEmpty, int hystrixStrategy) {

        // 获取缓存中的数据
        T t = getCacheAction.get();
        // 是否为空值
        if (ObjectUtils.nullSafeEquals(empty, t)) {
            // 为空值返回returnEmpty
            return returnEmpty;
        }
        // 判断缓存中的数据是否过期[或失效]
        if (!expiredAction.apply(t)) {
            return t;
        }
        // 获取默认值
        T defaultValue = getDefaultValue(getCacheAction, dataSourceAction, hystrixStrategy, empty, returnEmpty);
        try {
            /** 上分布式锁，防止缓存击穿 */
            T newT = redisLockerTemplate.tryLock(lockKey + ":" + empty.getClass(), () -> {
                // 获取缓存中的数据
                T v = getCacheAction.get();
                // 是否为空值
                if (ObjectUtils.nullSafeEquals(v, empty)) {
                    // 为空值返回returnEmpty
                    return returnEmpty;
                }
                // 判断缓存中的数据是否过期[或失效]
                if (!expiredAction.apply(v)) {
                    // 未过期，直接返回
                    return v;
                }
                // 去db中获取数据
                v = dataSourceAction.get();
                /** 如获取数据为null, 放入empty数据，防止缓存穿透 */
                if (v == null) {
                    // 放入缓存
                    setCacheAction.accept(empty);
                    // 返回returnEmpty
                    return returnEmpty;
                } else {
                    // 放入缓存
                    setCacheAction.accept(v);
                    // 返回数据
                    return v;
                }
            }, () -> defaultValue);
            return newT;
        } catch (Exception e) {
            log.error("ifExpired4ReGet", e);
            return defaultValue;
        }
    }

    private <T> T getDefaultValue(Supplier<T> getCacheAction, Supplier<T> dataSourceAction, int hystrixStrategy, T empty, T returnEmpty) {
        // 保底策略
        Supplier<T> strategy = hystrixStrategy == Constants.STRATEGY_FOR_DATA_SOURCE ? dataSourceAction : getCacheAction;

        T defaultValue = strategy.get();

        defaultValue = ObjectUtils.nullSafeEquals(empty, defaultValue) ? returnEmpty : defaultValue;

        return defaultValue;
    }


    /**
     * 缓存过期，重新获取数据的请求体
     */
    @Data
    @Accessors(chain = true)
    public static class CacheExpiredReGetRequest<T> {

        /**
         * 获取缓存数据的操作
         */
        private Supplier<T> getCacheAction;

        /**
         * 判断该缓存数据是否失效或过期
         */
        private Function<T, Boolean> expiredAction;

        /**
         * 从数据源获取数据的操作
         */
        private Supplier<T> dataSourceAction;

        /**
         * 将数据缓存的行为
         */
        private Consumer<T> setCacheAction;

        /**
         * 锁key
         */
        private String lockKey;

        /**
         * 定义的空值，当从数据源获取不到数据时，会将该空值存储到缓存中，但是返回 {@link #returnEmpty}
         */
        private T empty;

        /**
         * 当获取的数据为{@link #empty}时，返回 {@link #returnEmpty}
         */
        private T returnEmpty;

        /**
         * 保底策略，默认为0
         * <p>
         * 分布式锁超时的保底方案
         * <p>
         * 0-从缓存中获取，1-从数据源中获取
         */
        private int hystrixStrategy = 0;


    }

    public interface Constants {

        /**
         * 保底策略-缓存
         */
        int STRATEGY_FOR_CACHE = 0;

        /**
         * 保底策略-数据源
         */
        int STRATEGY_FOR_DATA_SOURCE = 1;

    }

}
