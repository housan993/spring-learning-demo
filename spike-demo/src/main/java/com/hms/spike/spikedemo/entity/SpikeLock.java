package com.hms.spike.spikedemo.entity;

import com.hms.spike.spikedemo.base.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "spike_lock")
public class SpikeLock extends BaseEntity {
    /**
     * 自增索引id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "spike_index")
    private Integer spikeIndex;

    /**
     * 锁名key值
     */
    @Column(name = "spike_lock_name")
    private String spikeLockName;

    /**
     * 计数器
     */
    @Column(name = "spike_count")
    private Integer spikeCount;

    /**
     * 锁过期时间
     */
    @Column(name = "spike_deadline")
    private Date spikeDeadline;

    /**
     * 值/描述
     */
    @Column(name = "spike_desc")
    private String spikeDesc;

    /**
     * 创建时间
     */
    @Column(name = "spike_create_time")
    private Date spikeCreateTime;

    /**
     * 修改时间
     */
    @Column(name = "spike_modify_time")
    private Date spikeModifyTime;

    /**
     * 记录状态，0：无效，1：有效
     */
    @Column(name = "spike_status")
    private Byte spikeStatus;

    /**
     * 获取自增索引id
     *
     * @return spike_index - 自增索引id
     */
    public Integer getSpikeIndex() {
        return spikeIndex;
    }

    /**
     * 设置自增索引id
     *
     * @param spikeIndex 自增索引id
     */
    public void setSpikeIndex(Integer spikeIndex) {
        this.spikeIndex = spikeIndex;
    }

    /**
     * 获取锁名key值
     *
     * @return spike_lock_name - 锁名key值
     */
    public String getSpikeLockName() {
        return spikeLockName;
    }

    /**
     * 设置锁名key值
     *
     * @param spikeLockName 锁名key值
     */
    public void setSpikeLockName(String spikeLockName) {
        this.spikeLockName = spikeLockName;
    }

    /**
     * 获取计数器
     *
     * @return spike_count - 计数器
     */
    public Integer getSpikeCount() {
        return spikeCount;
    }

    /**
     * 设置计数器
     *
     * @param spikeCount 计数器
     */
    public void setSpikeCount(Integer spikeCount) {
        this.spikeCount = spikeCount;
    }

    /**
     * 获取锁过期时间
     *
     * @return spike_deadline - 锁过期时间
     */
    public Date getSpikeDeadline() {
        return spikeDeadline;
    }

    /**
     * 设置锁过期时间
     *
     * @param spikeDeadline 锁过期时间
     */
    public void setSpikeDeadline(Date spikeDeadline) {
        this.spikeDeadline = spikeDeadline;
    }

    /**
     * 获取值/描述
     *
     * @return spike_desc - 值/描述
     */
    public String getSpikeDesc() {
        return spikeDesc;
    }

    /**
     * 设置值/描述
     *
     * @param spikeDesc 值/描述
     */
    public void setSpikeDesc(String spikeDesc) {
        this.spikeDesc = spikeDesc;
    }

    /**
     * 获取创建时间
     *
     * @return spike_create_time - 创建时间
     */
    public Date getSpikeCreateTime() {
        return spikeCreateTime;
    }

    /**
     * 设置创建时间
     *
     * @param spikeCreateTime 创建时间
     */
    public void setSpikeCreateTime(Date spikeCreateTime) {
        this.spikeCreateTime = spikeCreateTime;
    }

    /**
     * 获取修改时间
     *
     * @return spike_modify_time - 修改时间
     */
    public Date getSpikeModifyTime() {
        return spikeModifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param spikeModifyTime 修改时间
     */
    public void setSpikeModifyTime(Date spikeModifyTime) {
        this.spikeModifyTime = spikeModifyTime;
    }

    /**
     * 获取记录状态，0：无效，1：有效
     *
     * @return spike_status - 记录状态，0：无效，1：有效
     */
    public Byte getSpikeStatus() {
        return spikeStatus;
    }

    /**
     * 设置记录状态，0：无效，1：有效
     *
     * @param spikeStatus 记录状态，0：无效，1：有效
     */
    public void setSpikeStatus(Byte spikeStatus) {
        this.spikeStatus = spikeStatus;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}