package com.hms.spike.spikedemo.entity;

import com.hms.spike.spikedemo.base.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "red_packet_child")
public class RedPacketChild extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "red_packet_id")
    private Integer redPacketId;

    private Integer money;

    private Date deadline;

    private String status;

    @Column(name = "user_id")
    private Integer userId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return red_packet_id
     */
    public Integer getRedPacketId() {
        return redPacketId;
    }

    /**
     * @param redPacketId
     */
    public void setRedPacketId(Integer redPacketId) {
        this.redPacketId = redPacketId;
    }

    /**
     * @return money
     */
    public Integer getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(Integer money) {
        this.money = money;
    }

    /**
     * @return deadline
     */
    public Date getDeadline() {
        return deadline;
    }

    /**
     * @param deadline
     */
    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return user_id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}