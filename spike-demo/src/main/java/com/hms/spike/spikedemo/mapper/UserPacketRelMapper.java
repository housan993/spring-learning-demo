package com.hms.spike.spikedemo.mapper;

import com.hms.spike.spikedemo.entity.RedPacketChild;
import com.hms.spike.spikedemo.entity.SpikeLock;
import com.hms.spike.spikedemo.entity.UserPacketRel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

public interface UserPacketRelMapper extends Mapper<UserPacketRel> {

    @Select("SELECT * from user_packet_rel WHERE red_packet_id = #{redId} and user_id = #{userId} ")
    UserPacketRel selectByRedIdAndUserId(@Param("redId") Integer redId, @Param("userId") Integer userId);
}