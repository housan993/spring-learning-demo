package com.hms.spike.spikedemo.entity;

import com.hms.spike.spikedemo.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "user_packet_rel")
public class UserPacketRel extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "red_packet_id")
    private Integer redPacketId;

    @Column(name = "red_packet_child_id")
    private Integer redPacketChildId;

    @Column(name = "user_id")
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(Integer redPacketId) {
        this.redPacketId = redPacketId;
    }

    public Integer getRedPacketChildId() {
        return redPacketChildId;
    }

    public void setRedPacketChildId(Integer redPacketChildId) {
        this.redPacketChildId = redPacketChildId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}