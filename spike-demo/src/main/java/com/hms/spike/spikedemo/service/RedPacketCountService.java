package com.hms.spike.spikedemo.service;


/**
 *
 *
 * @author hms
 * @date 2019/1/3 14:32
 */
public interface RedPacketCountService {


    /**
     * 更新红包用户id
     * @param id
     * @param userId
     */
    void updatePacketChild(Integer id, Integer userId);

}
