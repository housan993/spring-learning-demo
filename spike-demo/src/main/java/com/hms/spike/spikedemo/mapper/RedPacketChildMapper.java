package com.hms.spike.spikedemo.mapper;

import com.hms.spike.spikedemo.entity.RedPacketChild;
import com.hms.spike.spikedemo.entity.SpikeLock;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface RedPacketChildMapper extends Mapper<RedPacketChild> {

	/*CUSTOM_CODE_START*/

    @Select("SELECT * from red_packet_child WHERE red_packet_id = #{redPacketId} and status = 1 ")
    List<RedPacketChild> selectByRedPacketId(@Param("redPacketId") Integer redPacketId);
	/*CUSTOM_CODE_END*/
}