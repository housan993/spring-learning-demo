package com.hms.spike.spikedemo.web.criteria;

import com.hms.spike.spikedemo.base.BaseEntity;

/**
 * @author hms
 * @date 2019/1/7 11:24
 */
public class GetLockRequest extends BaseEntity {
    private String lockName;
    private Long expireTime;
    private String desc;

    public String getLockName() {
        return lockName;
    }

    public void setLockName(String lockName) {
        this.lockName = lockName;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
