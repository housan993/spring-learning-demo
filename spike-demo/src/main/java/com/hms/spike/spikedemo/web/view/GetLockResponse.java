package com.hms.spike.spikedemo.web.view;

import com.hms.spike.spikedemo.base.BaseEntity;

/**
 * @author hms
 * @date 2019/1/7 11:21
 */
public class GetLockResponse extends BaseEntity {
    private int lock;
    private int num;

    public int getLock() {
        return lock;
    }

    public void setLock(int lock) {
        this.lock = lock;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
