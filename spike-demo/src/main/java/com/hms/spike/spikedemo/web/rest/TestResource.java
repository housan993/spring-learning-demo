package com.hms.spike.spikedemo.web.rest;


import com.google.common.collect.Maps;
import com.hms.spike.spikedemo.service.RedPacketService;
import com.hms.spike.spikedemo.service.SpikeLockService;
import com.hms.spike.spikedemo.web.criteria.GrabPacketCriteria;
import com.hms.spike.spikedemo.web.view.GrabPacketView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hms
 * @date 2018/12/26 14:28
 */

@RestController
@RequestMapping("test")
public class TestResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedPacketService redPacketService;

    @Resource
    private SpikeLockService spikeLockService;


    @PostMapping("/send-packet")
    public ResponseEntity<Map<String, Object>> sendPacket(@RequestBody Map<String, Object> res) {
        HashMap<String, Object> map = Maps.newHashMap();

        log.info("res={}", res);
        String name = (String) res.get("packetName");
        String money = (String) res.get("money");
        String count = (String) res.get("count");

        BigDecimal bigDecimalMoney = new BigDecimal(money);
        BigDecimal bigDecimalCount = new BigDecimal(count);

        Integer redId = redPacketService.sendPacket(name,
                bigDecimalMoney.intValue(), bigDecimalCount.intValue());

        map.put("code", "000000");
        map.put("data", redId);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/grab-packet")
    public ResponseEntity<Map<String, Object>> grabPacket(@RequestBody GrabPacketCriteria grabPacketCriteria) {
        HashMap<String, Object> map = Maps.newHashMap();

        GrabPacketView grabPacketView = spikeLockService.grabPacket(grabPacketCriteria);

        map.put("code", "000000");
        map.put("data", grabPacketView);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/mock/grab-packet")
    public ResponseEntity<Map<String, Object>> mockGrabPacket(@RequestBody GrabPacketCriteria grabPacketCriteria) {
        HashMap<String, Object> map = Maps.newHashMap();

        spikeLockService.mockGrabPacket(grabPacketCriteria);

        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/mysql/siege/{num}")
    public ResponseEntity<String> mysqlSiege(@PathVariable Integer num) {
        spikeLockService.mysqlSiege(num);
        return new ResponseEntity<>("压测结束", HttpStatus.OK);
    }

}
