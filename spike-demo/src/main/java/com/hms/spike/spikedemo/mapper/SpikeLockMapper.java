package com.hms.spike.spikedemo.mapper;

import com.hms.spike.spikedemo.entity.SpikeLock;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

public interface SpikeLockMapper extends Mapper<SpikeLock> {

	/*CUSTOM_CODE_START*/

    /**
     * 中央锁
     * @param spikeLockName
     * @return
     */
    @Select("SELECT * from spike_lock WHERE spike_lock_name = #{spikeLockName} for update ")
    SpikeLock selectForUpdate(@Param("spikeLockName") String spikeLockName);

    /**
     * 根据中央锁key查询信息
     * @param spikeLockName
     * @return
     */
    @Select("SELECT * from spike_lock WHERE spike_lock_name = #{spikeLockName} ")
    SpikeLock selectBySpikeLockName(@Param("spikeLockName") String spikeLockName);

    @Update("update spike_lock set spike_count = spike_count - 1 WHERE spike_index = #{spikeIndex} and spike_count > 0 ")
    void updateCounter(@Param("spikeIndex") Integer spikeIndex);

	/*CUSTOM_CODE_END*/
}