### 压测
# 工具 siege
+ sudo apt install siege

# 基本用法
+ siege -b -c100 -t10S -T "application/json" 'http://localhost:8080/test/grab-packet POST {"userId":"5","redId":5}'

# 使用
+ 这是发了600个红包,开了600个线程600个用户同时去抢红包,测试QPS
+ siege -b -c600 -r1 -T "application/json" -f ./data/urls.txt

```txt
hms@hms-ubuntu ~> siege -b -c600 -r1 -T "application/json" -f ./data/urls.txt
** SIEGE 4.0.4
** Preparing 600 concurrent users for battle.
The server is now under siege...
Transactions:		         600 hits
Availability:		      100.00 %
Elapsed time:		        6.07 secs
Data transferred:	        0.08 MB
Response time:		        3.98 secs
Transaction rate:	       98.85 trans/sec
Throughput:		        0.01 MB/sec
Concurrency:		      392.97
Successful transactions:         600
Failed transactions:	           0
Longest transaction:	        6.04
Shortest transaction:	        0.24


```