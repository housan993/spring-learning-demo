package com.hms.cloud.zipkinclienttracedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZipkinClientTraceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinClientTraceDemoApplication.class, args);
    }
}
