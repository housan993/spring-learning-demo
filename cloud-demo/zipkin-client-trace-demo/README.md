###zipkin-client-trace-demo

首先启动zipkin-server
启动方式参考地址
https://windmt.com/2018/04/24/spring-cloud-12-sleuth-zipkin/

docker 启动方式
docker run -d -p 9604:9411 --name zipkin-server-9604 -e RABBIT_ADDRESSES=10.0.75.1:5672 
-e RABBIT_PASSWORD=root -e RABBIT_USER=root openzipkin/zipkin

访问http://localhost:9602/trace-a


需要集成mq时需要
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-stream-binder-rabbit</artifactId>
</dependency>

且
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-starter-parent</artifactId>
<version>2.1.0.RELEASE</version>
必须是2.1.0因为2.1.1会报错

集成mq后可以不用配置zipkin
zipkin:
    base-url: http://localhost:9902/
所以这里配置了个错误地址

当不需要集成mq时只需要
sleuth:
    web:
      client:
        enabled: true
    sampler:
      probability: 1.0 # 将采样比例设置为 1.0，也就是全部都需要。默认是 0.1
  zipkin:
    base-url: http://localhost:9902/ # 指定了 Zipkin 服务器的地址
这样就行了，jar
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-sleuth</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>

附带zipkin docker
docker run -d -p 9901:9411 --name zipkin-server -e RABBIT_ADDRESSES=10.0.75.1:5672 -e RABBIT_PASSWORD=root -e RABBIT_USER=root openzipkin/zipkin
