package com.hms.cloud.zipkinclienttracebdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZipkinClientTracebDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinClientTracebDemoApplication.class, args);
    }
}
