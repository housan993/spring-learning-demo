package com.hms.cloud.zipkinclienttracebdemo.web.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @author Administrator
 */
@RestController
public class TracebController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/trace-b")
    public Mono<String> trace() {
        System.out.println("===call trace-b===" + port);

        return Mono.just("Trace");
    }
}
