###eureka-server-demo

修改 hosts 文件中的配置
127.0.0.1 peer1 peer2 peer3

mvn clean package  -Dmaven.test.skip=true

java -jar target/eureka-server-0.0.1-SNAPSHOT.jar  --spring.profiles.active=peer1
java -jar target/eureka-server-0.0.1-SNAPSHOT.jar  --spring.profiles.active=peer2
java -jar target/eureka-server-0.0.1-SNAPSHOT.jar  --spring.profiles.active=peer3

###注意问题点未解决
访问地址 http://localhost:7001/
而不是   http://localhost:7001/eureka/
