###zuul-demo

这里的配置表示，访问/it/** 直接重定向到http://www.ityouknow.com/**
zuul.routes.9401.path=/it/**
zuul.routes.9401.url=http://www.ityouknow.com/
例: localhost:9401/it/ 

zuul.routes.api-a.path=/producer/**
zuul.routes.api-a.serviceId=spring-cloud-producer
例: localhost:9401/producer/hello?name="123"&token="123"

当不配置时zuul自动结合注册中心
例: localhost:9401/spring-cloud-producer/hello?name="123"&token="123"

附带zipkin docker
docker run -d -p 9901:9411 --name zipkin-server -e RABBIT_ADDRESSES=10.0.75.1:5672 -e RABBIT_PASSWORD=root -e RABBIT_USER=root openzipkin/zipkin
