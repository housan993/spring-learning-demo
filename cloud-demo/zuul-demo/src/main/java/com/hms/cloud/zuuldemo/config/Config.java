package com.hms.cloud.zuuldemo.config;

import com.hms.cloud.zuuldemo.filter.TokenFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean
    public TokenFilter tokenFilter() {
        return new TokenFilter();
    }
}
