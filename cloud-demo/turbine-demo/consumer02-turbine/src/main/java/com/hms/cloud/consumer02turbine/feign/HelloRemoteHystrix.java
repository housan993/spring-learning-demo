package com.hms.cloud.consumer02turbine.feign;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class HelloRemoteHystrix implements HelloRemote {

    @Override
    public String hello(@RequestParam(value = "name") String name) {
        return "hello-02" + name + ", this message send failed ";
    }

    @Override
    public String test(String name) {
        return "Test-02" + name + ", this message send failed ";
    }
}
