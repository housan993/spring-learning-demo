package com.hms.cloud.consumer02turbine.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Administrator
 */
@FeignClient(name = "producer02", fallback = HelloRemoteHystrix.class)
public interface HelloRemote {
    /**
     * 测试调用
     *
     * @param name 名字
     * @return 字符串
     */
    @RequestMapping(value = "/hello")
    String hello(@RequestParam(value = "name") String name);

    /**
     * 测试调用
     *
     * @param name 名字
     * @return 字符串
     */
    @GetMapping(value = "/test")
    String test(@RequestParam(value = "name") String name);
}
