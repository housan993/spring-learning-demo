package com.hms.cloud.producer02turbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Producer02TurbineApplication {

    public static void main(String[] args) {
        SpringApplication.run(Producer02TurbineApplication.class, args);
    }
}
