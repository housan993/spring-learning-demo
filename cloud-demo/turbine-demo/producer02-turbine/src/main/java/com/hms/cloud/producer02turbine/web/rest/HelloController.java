package com.hms.cloud.producer02turbine.web.rest;

import com.fasterxml.jackson.core.filter.TokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 */
@RestController
public class HelloController {
    private final Logger logger = LoggerFactory.getLogger(TokenFilter.class);

    @Value("${server.port}")
    private String port;

    @RequestMapping("/hello")
    public String index(@RequestParam String name) {
        logger.info("name={}, port={}", name, port);
        return "hello " + name + ",this is producer02 message,port=" + port;
    }

    @GetMapping("/test")
    public String hello(@RequestParam String name) {
        return "Hello, " + name + "!";
    }
}
