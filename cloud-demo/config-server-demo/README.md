###获取配置信息格式

仓库中的配置文件会被转换成web接口，访问可以参照以下的规则：

####/{application}/{profile}[/{label}]
####/{application}-{profile}.yml
####/{label}/{application}-{profile}.yml
####/{application}-{profile}.properties
####/{label}/{application}-{profile}.properties


###注意
  profiles:
    active: native
    
  native:
    search-locations: classpath:/config-repo
    
默认是配置git中心玩，现在本地玩，本地玩就得配置上面2个配置

每次修改本地配置或者git配置之后需要调用
curl -X POST http://localhost:9501/actuator/bus-refresh/
这样客户端才会刷新最新配置
或者局部刷新功能
curl -X POST http://localhost:9501/actuator/bus-refresh/config-client-9504
curl -X POST http://localhost:9501/actuator/bus-refresh/config-client:9502

这样看来还是百度的disconfig比较给力
或者是不会玩