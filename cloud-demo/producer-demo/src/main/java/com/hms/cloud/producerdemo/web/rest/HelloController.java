package com.hms.cloud.producerdemo.web.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 */
@RestController
public class HelloController {
    private final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @Value("${server.port}")
    private String port;

    @RequestMapping("/hello")
    public String index(@RequestParam String name) {
        logger.info("name={}, port={}", name, port);
        return "hello " + name + ",this is producer01 message,port=" + port;
    }

    @GetMapping("/test")
    public String hello(@RequestParam String name) {
        return "Hello, " + name + "!";
    }
}
