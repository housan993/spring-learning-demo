package com.hms.cloud.consumerdemo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Administrator
 */
@FeignClient(name = "producer", fallback = HelloRemoteHystrix.class)
public interface HelloRemote {
    /**
     * 测试调用
     *
     * @param name 名字
     * @return 字符串
     */
    @RequestMapping(value = "/hello")
    String hello(@RequestParam(value = "name") String name);

    /**
     * 测试调用
     *
     * @param name 名字
     * @return 字符串
     */
    @GetMapping(value = "/test")
    String test(@RequestParam(value = "name") String name);
}
