package com.hms.webflux.webfluxdemo.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @author hms
 * @date 2019/2/14 9:42
 */
@RestController
public class HelloController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/hello")
    public Mono<String> hello() {
        log.info("hello进来了");
        return Mono.just("Welcome to reactive world ~");
    }
}
