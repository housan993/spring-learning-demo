package com.hms.webflux.webfluxdemo.web.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = HelloController.class)
public class HelloControllerTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WebTestClient client;

    @Test
    public void getHello() {
        log.info("getHello测试");
        client.get().uri("/hello").exchange().expectStatus().isOk();
    }
}