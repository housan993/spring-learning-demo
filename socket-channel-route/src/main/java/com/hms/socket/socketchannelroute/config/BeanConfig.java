package com.hms.socket.socketchannelroute.config;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hms.utils.route.algorithm.RouteHandle;
import com.hms.utils.route.algorithm.consistenthash.AbstractConsistentHash;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;

/**
 * @author hms
 */
@Configuration
public class BeanConfig {

    private static final String ROUTE_HANDLE_CONSISTENT_HASH = "ConsistentHash";

    private static final String METHOD_SET_HASH = "setHash";

    @Value("${route-consistent-hash.handle:com.hms.utils.route.algorithm.consistenthash.ConsistentHashHandle}")
    private String handle;

    @Value("${route-consistent-hash.consistentHash:com.hms.utils.route.algorithm.consistenthash.TreeMapConsistentHash}")
    private String consistentHash;

    @Bean
    public LoadingCache<String, String> buildCache() {
        return CacheBuilder.newBuilder()
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(@NotNull String s) {
                        return null;
                    }
                });
    }

    @Bean
    public RouteHandle buildRouteHandle() throws Exception {
        String routeWay = handle;
        RouteHandle routeHandle = (RouteHandle) Class.forName(routeWay).newInstance();
        if (routeWay.contains(ROUTE_HANDLE_CONSISTENT_HASH)) {
            //一致性 hash 算法
            Method method = Class.forName(routeWay).getMethod(METHOD_SET_HASH, AbstractConsistentHash.class);
            AbstractConsistentHash abstractConsistentHash = (AbstractConsistentHash)
                    Class.forName(consistentHash).newInstance();
            method.invoke(routeHandle, abstractConsistentHash);
            return routeHandle;
        } else {
            return routeHandle;
        }
    }
}
