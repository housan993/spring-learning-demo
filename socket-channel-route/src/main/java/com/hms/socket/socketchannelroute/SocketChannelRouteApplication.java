package com.hms.socket.socketchannelroute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketChannelRouteApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocketChannelRouteApplication.class, args);
    }

}
