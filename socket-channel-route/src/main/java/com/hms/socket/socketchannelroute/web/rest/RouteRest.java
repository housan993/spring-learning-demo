package com.hms.socket.socketchannelroute.web.rest;

import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import com.hms.socket.socketchanelcommon.vo.criteria.GoogleProtocolCriteria;
import com.hms.socket.socketchannelroute.service.ServerCacheService;
import com.hms.utils.base.BaseController;
import com.hms.utils.base.BaseView;
import com.hms.utils.route.algorithm.RouteHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hms
 */
@RestController
@RequestMapping("route")
public class RouteRest extends BaseController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServerCacheService serverCacheService;

    @Autowired
    private RouteHandle routeHandle;


    /**
     * 服务端注册
     *
     * @param socketServerInfo
     * @return
     */
    @PostMapping("/node-register")
    public ResponseEntity<BaseView> nodeRegister(@RequestBody SocketServerInfo socketServerInfo) {
        log.info("请求信息={}", socketServerInfo);
        serverCacheService.addCache(socketServerInfo.getServerIp(), socketServerInfo.getServerNettyPort(),
                socketServerInfo.getServerHttpPort());
        BaseView baseView = new BaseView("服务端注册成功");
        return new ResponseEntity<>(baseView, HttpStatus.OK);
    }

    /**
     * 根据用户id分配服务器节点
     *
     * @param googleProtocolCriteria
     * @return
     */
    @PostMapping("/distribution")
    public ResponseEntity<BaseView> distribution(@RequestBody GoogleProtocolCriteria googleProtocolCriteria) {
        log.info("登录信息={}", googleProtocolCriteria.toString());
        List<String> cacheServiceList = serverCacheService.getAll();
        if (CollectionUtils.isEmpty(cacheServiceList)) {
            BaseView baseView = new BaseView("服务器节点信息不存在");
            return new ResponseEntity<>(baseView, HttpStatus.SEE_OTHER);
        }
        String server = routeHandle.routeServer(cacheServiceList,
                String.valueOf(googleProtocolCriteria.getUserId()));

        SocketServerInfo serverInfo = serverCacheService.analysisCache(server);
        BaseView baseView = new BaseView("登录成功");
        baseView.setData(serverInfo);
        return new ResponseEntity<>(baseView, HttpStatus.OK);
    }

}
