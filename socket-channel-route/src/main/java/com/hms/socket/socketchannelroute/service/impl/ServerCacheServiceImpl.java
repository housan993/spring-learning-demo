package com.hms.socket.socketchannelroute.service.impl;

import com.google.common.base.Splitter;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import com.hms.socket.socketchannelroute.service.ServerCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author hms
 */
@Service
public class ServerCacheServiceImpl implements ServerCacheService {

    private final static Logger log = LoggerFactory.getLogger(ServerCacheServiceImpl.class);

    @Autowired
    private LoadingCache<String, String> cache;

    @Override
    public void addCache(String key) {
        cache.put(key, key);
    }

    @Override
    public void addCache(String serverIp, int serverNettyPort, int serverPort) {
        addCache(serverIp + ":" + serverNettyPort + ":" + serverPort);
    }

    @Override
    public List<String> getAll() {
        List<String> list = Lists.newArrayList();

        if (cache.size() == 0) {
            log.error("没有服务注册");
            return list;
        }
        for (Map.Entry<String, String> entry : cache.asMap().entrySet()) {
            list.add(entry.getKey());
        }
        return list;
    }

    @Override
    public SocketServerInfo analysisCache(String key) {
        SocketServerInfo serverInfo = new SocketServerInfo();
        List<String> result = Splitter.on(":").trimResults().splitToList(key);
        if (CollectionUtils.isEmpty(result)
                || result.size() < 3) {
            log.error("服务端信息解析失败");
            return serverInfo;
        }
        serverInfo.setServerIp(result.get(0));
        serverInfo.setServerNettyPort(Integer.valueOf(result.get(1)));
        serverInfo.setServerHttpPort(Integer.valueOf(result.get(2)));
        return serverInfo;
    }
}
