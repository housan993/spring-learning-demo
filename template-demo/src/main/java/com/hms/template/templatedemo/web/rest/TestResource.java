package com.hms.template.templatedemo.web.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author hms
 * @date 2018/12/26 14:28
 */

@RestController
@RequestMapping("test")
public class TestResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/resource-post")
    public ResponseEntity<Map<String, Object>> resourcePost(@RequestBody Map<String, Object> map) {
        log.info("map={}", map);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/resource-get")
    public Map<String, Object> resourceGet(@RequestBody Map<String, Object> map) {
        log.info("map={}", map);
        return map;
    }

}
