package com.hms.socket.socketchanelcommon.enums;

/**
 * @author hms
 */

public enum SymbolEnum {
    /**
     * 逗号
     */
    COMMA(",");

    private String symbol;

    SymbolEnum(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
