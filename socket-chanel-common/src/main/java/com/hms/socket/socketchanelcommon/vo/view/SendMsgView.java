package com.hms.socket.socketchanelcommon.vo.view;


import com.hms.utils.base.BaseEntity;

/**
 *
 * @author hms
 */
public class SendMsgView extends BaseEntity {
    private String msg ;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
