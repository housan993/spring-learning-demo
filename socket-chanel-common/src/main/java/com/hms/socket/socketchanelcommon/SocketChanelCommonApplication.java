package com.hms.socket.socketchanelcommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketChanelCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocketChanelCommonApplication.class, args);
    }

}
