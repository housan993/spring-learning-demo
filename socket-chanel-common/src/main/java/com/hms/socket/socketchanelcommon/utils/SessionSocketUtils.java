package com.hms.socket.socketchanelcommon.utils;

import com.hms.socket.socketchanelcommon.entity.SocketUsrInfo;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hms
 */
public class SessionSocketUtils {
    /**
     * 客户端用户id与客户端channel
     */
    private static final Map<Long, NioSocketChannel> CLIENT_CHANNEL_MAP = new ConcurrentHashMap<>(16);
    /**
     * 客户端用户id与服务端channel
     */
    private static final Map<Long, NioSocketChannel> SERVER_CHANNEL_MAP = new ConcurrentHashMap<>(16);
    /**
     * 客户端id与客户名称
     */
    private static final Map<Long, String> SESSION_MAP = new ConcurrentHashMap<>(16);

    public static void saveSession(Long userId, String userName) {
        SESSION_MAP.put(userId, userName);
    }

    public static void removeSession(Long userId) {
        SESSION_MAP.remove(userId);
    }

    public static void put(Long id, NioSocketChannel socketChannel) {
        CLIENT_CHANNEL_MAP.put(id, socketChannel);
    }

    public static NioSocketChannel get(Long id) {
        return CLIENT_CHANNEL_MAP.get(id);
    }

    public static Map<Long, NioSocketChannel> getMap() {
        return CLIENT_CHANNEL_MAP;
    }

    public static void remove(NioSocketChannel nioSocketChannel) {
        CLIENT_CHANNEL_MAP.entrySet().stream()
                .filter(entry -> entry.getValue() == nioSocketChannel)
                .forEach(entry -> CLIENT_CHANNEL_MAP.remove(entry.getKey()));
        SERVER_CHANNEL_MAP.entrySet().stream()
                .filter(entry -> entry.getValue() == nioSocketChannel)
                .forEach(entry -> SERVER_CHANNEL_MAP.remove(entry.getKey()));
    }

    /**
     * 获取注册用户信息
     *
     * @param nioSocketChannel
     * @return
     */
    public static SocketUsrInfo getUserId(NioSocketChannel nioSocketChannel) {
        for (Map.Entry<Long, NioSocketChannel> entry : CLIENT_CHANNEL_MAP.entrySet()) {
            NioSocketChannel value = entry.getValue();
            if (nioSocketChannel == value) {
                Long key = entry.getKey();
                String userName = SESSION_MAP.get(key);
                return new SocketUsrInfo()
                        .setUserId(key)
                        .setUserName(userName);
            }
        }

        return null;
    }

    public static void putServerChannel(Long id, NioSocketChannel severChannel) {
        SERVER_CHANNEL_MAP.put(id, severChannel);
    }

    public static NioSocketChannel getServerChannel(Long id) {
        return SERVER_CHANNEL_MAP.get(id);
    }

    public static SocketUsrInfo getUserInfoByServerChannel(NioSocketChannel serverChannel) {
        for (Map.Entry<Long, NioSocketChannel> entry : SERVER_CHANNEL_MAP.entrySet()) {
            NioSocketChannel value = entry.getValue();
            if (serverChannel == value) {
                Long key = entry.getKey();
                String userName = SESSION_MAP.get(key);
                return new SocketUsrInfo()
                        .setUserId(key)
                        .setUserName(userName);
            }
        }
        return null;
    }
}
