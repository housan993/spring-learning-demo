package com.hms.socket.socketchanelcommon.entity;


import com.hms.utils.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *
 * @author hms
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class SocketServerInfo extends BaseEntity {
    private String serverIp ;
    private Integer serverNettyPort;
    private Integer serverHttpPort;
}
