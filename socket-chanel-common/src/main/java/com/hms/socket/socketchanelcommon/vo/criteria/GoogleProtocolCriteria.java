package com.hms.socket.socketchanelcommon.vo.criteria;


import com.hms.utils.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author hms
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GoogleProtocolCriteria extends BaseEntity {
    private Integer requestId;

    private String msg;
    private String msgContentType;

    private Long userId;
    private String userName;

    private LocalDateTime createTime;

}
