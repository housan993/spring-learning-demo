// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: SocketChanelResponse.proto

package com.hms.socket.socketchanelcommon.common.protocol;

public final class SocketChanelResponseProto {
  private SocketChanelResponseProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface SocketChanelResponseOrBuilder extends
      // @@protoc_insertion_point(interface_extends:protocol.SocketChanelResponse)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>int64 responseId = 2;</code>
     * @return The responseId.
     */
    long getResponseId();

    /**
     * <code>string resMsg = 1;</code>
     * @return The resMsg.
     */
    java.lang.String getResMsg();
    /**
     * <code>string resMsg = 1;</code>
     * @return The bytes for resMsg.
     */
    com.google.protobuf.ByteString
        getResMsgBytes();

    /**
     * <code>int32 type = 3;</code>
     * @return The type.
     */
    int getType();

    /**
     * <code>.google.protobuf.Timestamp sendTime = 4;</code>
     * @return Whether the sendTime field is set.
     */
    boolean hasSendTime();
    /**
     * <code>.google.protobuf.Timestamp sendTime = 4;</code>
     * @return The sendTime.
     */
    com.google.protobuf.Timestamp getSendTime();
    /**
     * <code>.google.protobuf.Timestamp sendTime = 4;</code>
     */
    com.google.protobuf.TimestampOrBuilder getSendTimeOrBuilder();
  }
  /**
   * Protobuf type {@code protocol.SocketChanelResponse}
   */
  public  static final class SocketChanelResponse extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:protocol.SocketChanelResponse)
      SocketChanelResponseOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use SocketChanelResponse.newBuilder() to construct.
    private SocketChanelResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private SocketChanelResponse() {
      resMsg_ = "";
    }

    @java.lang.Override
    @SuppressWarnings({"unused"})
    protected java.lang.Object newInstance(
        UnusedPrivateParameter unused) {
      return new SocketChanelResponse();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private SocketChanelResponse(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              java.lang.String s = input.readStringRequireUtf8();

              resMsg_ = s;
              break;
            }
            case 16: {

              responseId_ = input.readInt64();
              break;
            }
            case 24: {

              type_ = input.readInt32();
              break;
            }
            case 34: {
              com.google.protobuf.Timestamp.Builder subBuilder = null;
              if (sendTime_ != null) {
                subBuilder = sendTime_.toBuilder();
              }
              sendTime_ = input.readMessage(com.google.protobuf.Timestamp.parser(), extensionRegistry);
              if (subBuilder != null) {
                subBuilder.mergeFrom(sendTime_);
                sendTime_ = subBuilder.buildPartial();
              }

              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.internal_static_protocol_SocketChanelResponse_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.internal_static_protocol_SocketChanelResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.class, com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.Builder.class);
    }

    public static final int RESPONSEID_FIELD_NUMBER = 2;
    private long responseId_;
    /**
     * <code>int64 responseId = 2;</code>
     * @return The responseId.
     */
    public long getResponseId() {
      return responseId_;
    }

    public static final int RESMSG_FIELD_NUMBER = 1;
    private volatile java.lang.Object resMsg_;
    /**
     * <code>string resMsg = 1;</code>
     * @return The resMsg.
     */
    public java.lang.String getResMsg() {
      java.lang.Object ref = resMsg_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        resMsg_ = s;
        return s;
      }
    }
    /**
     * <code>string resMsg = 1;</code>
     * @return The bytes for resMsg.
     */
    public com.google.protobuf.ByteString
        getResMsgBytes() {
      java.lang.Object ref = resMsg_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        resMsg_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int TYPE_FIELD_NUMBER = 3;
    private int type_;
    /**
     * <code>int32 type = 3;</code>
     * @return The type.
     */
    public int getType() {
      return type_;
    }

    public static final int SENDTIME_FIELD_NUMBER = 4;
    private com.google.protobuf.Timestamp sendTime_;
    /**
     * <code>.google.protobuf.Timestamp sendTime = 4;</code>
     * @return Whether the sendTime field is set.
     */
    public boolean hasSendTime() {
      return sendTime_ != null;
    }
    /**
     * <code>.google.protobuf.Timestamp sendTime = 4;</code>
     * @return The sendTime.
     */
    public com.google.protobuf.Timestamp getSendTime() {
      return sendTime_ == null ? com.google.protobuf.Timestamp.getDefaultInstance() : sendTime_;
    }
    /**
     * <code>.google.protobuf.Timestamp sendTime = 4;</code>
     */
    public com.google.protobuf.TimestampOrBuilder getSendTimeOrBuilder() {
      return getSendTime();
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getResMsgBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 1, resMsg_);
      }
      if (responseId_ != 0L) {
        output.writeInt64(2, responseId_);
      }
      if (type_ != 0) {
        output.writeInt32(3, type_);
      }
      if (sendTime_ != null) {
        output.writeMessage(4, getSendTime());
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getResMsgBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, resMsg_);
      }
      if (responseId_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(2, responseId_);
      }
      if (type_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(3, type_);
      }
      if (sendTime_ != null) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(4, getSendTime());
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse)) {
        return super.equals(obj);
      }
      com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse other = (com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse) obj;

      if (getResponseId()
          != other.getResponseId()) return false;
      if (!getResMsg()
          .equals(other.getResMsg())) return false;
      if (getType()
          != other.getType()) return false;
      if (hasSendTime() != other.hasSendTime()) return false;
      if (hasSendTime()) {
        if (!getSendTime()
            .equals(other.getSendTime())) return false;
      }
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + RESPONSEID_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getResponseId());
      hash = (37 * hash) + RESMSG_FIELD_NUMBER;
      hash = (53 * hash) + getResMsg().hashCode();
      hash = (37 * hash) + TYPE_FIELD_NUMBER;
      hash = (53 * hash) + getType();
      if (hasSendTime()) {
        hash = (37 * hash) + SENDTIME_FIELD_NUMBER;
        hash = (53 * hash) + getSendTime().hashCode();
      }
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code protocol.SocketChanelResponse}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:protocol.SocketChanelResponse)
        com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponseOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.internal_static_protocol_SocketChanelResponse_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.internal_static_protocol_SocketChanelResponse_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.class, com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.Builder.class);
      }

      // Construct using com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        responseId_ = 0L;

        resMsg_ = "";

        type_ = 0;

        if (sendTimeBuilder_ == null) {
          sendTime_ = null;
        } else {
          sendTime_ = null;
          sendTimeBuilder_ = null;
        }
        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.internal_static_protocol_SocketChanelResponse_descriptor;
      }

      @java.lang.Override
      public com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse getDefaultInstanceForType() {
        return com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.getDefaultInstance();
      }

      @java.lang.Override
      public com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse build() {
        com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse buildPartial() {
        com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse result = new com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse(this);
        result.responseId_ = responseId_;
        result.resMsg_ = resMsg_;
        result.type_ = type_;
        if (sendTimeBuilder_ == null) {
          result.sendTime_ = sendTime_;
        } else {
          result.sendTime_ = sendTimeBuilder_.build();
        }
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse) {
          return mergeFrom((com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse other) {
        if (other == com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse.getDefaultInstance()) return this;
        if (other.getResponseId() != 0L) {
          setResponseId(other.getResponseId());
        }
        if (!other.getResMsg().isEmpty()) {
          resMsg_ = other.resMsg_;
          onChanged();
        }
        if (other.getType() != 0) {
          setType(other.getType());
        }
        if (other.hasSendTime()) {
          mergeSendTime(other.getSendTime());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private long responseId_ ;
      /**
       * <code>int64 responseId = 2;</code>
       * @return The responseId.
       */
      public long getResponseId() {
        return responseId_;
      }
      /**
       * <code>int64 responseId = 2;</code>
       * @param value The responseId to set.
       * @return This builder for chaining.
       */
      public Builder setResponseId(long value) {
        
        responseId_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>int64 responseId = 2;</code>
       * @return This builder for chaining.
       */
      public Builder clearResponseId() {
        
        responseId_ = 0L;
        onChanged();
        return this;
      }

      private java.lang.Object resMsg_ = "";
      /**
       * <code>string resMsg = 1;</code>
       * @return The resMsg.
       */
      public java.lang.String getResMsg() {
        java.lang.Object ref = resMsg_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          resMsg_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>string resMsg = 1;</code>
       * @return The bytes for resMsg.
       */
      public com.google.protobuf.ByteString
          getResMsgBytes() {
        java.lang.Object ref = resMsg_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          resMsg_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string resMsg = 1;</code>
       * @param value The resMsg to set.
       * @return This builder for chaining.
       */
      public Builder setResMsg(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        resMsg_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string resMsg = 1;</code>
       * @return This builder for chaining.
       */
      public Builder clearResMsg() {
        
        resMsg_ = getDefaultInstance().getResMsg();
        onChanged();
        return this;
      }
      /**
       * <code>string resMsg = 1;</code>
       * @param value The bytes for resMsg to set.
       * @return This builder for chaining.
       */
      public Builder setResMsgBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        resMsg_ = value;
        onChanged();
        return this;
      }

      private int type_ ;
      /**
       * <code>int32 type = 3;</code>
       * @return The type.
       */
      public int getType() {
        return type_;
      }
      /**
       * <code>int32 type = 3;</code>
       * @param value The type to set.
       * @return This builder for chaining.
       */
      public Builder setType(int value) {
        
        type_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>int32 type = 3;</code>
       * @return This builder for chaining.
       */
      public Builder clearType() {
        
        type_ = 0;
        onChanged();
        return this;
      }

      private com.google.protobuf.Timestamp sendTime_;
      private com.google.protobuf.SingleFieldBuilderV3<
          com.google.protobuf.Timestamp, com.google.protobuf.Timestamp.Builder, com.google.protobuf.TimestampOrBuilder> sendTimeBuilder_;
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       * @return Whether the sendTime field is set.
       */
      public boolean hasSendTime() {
        return sendTimeBuilder_ != null || sendTime_ != null;
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       * @return The sendTime.
       */
      public com.google.protobuf.Timestamp getSendTime() {
        if (sendTimeBuilder_ == null) {
          return sendTime_ == null ? com.google.protobuf.Timestamp.getDefaultInstance() : sendTime_;
        } else {
          return sendTimeBuilder_.getMessage();
        }
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      public Builder setSendTime(com.google.protobuf.Timestamp value) {
        if (sendTimeBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          sendTime_ = value;
          onChanged();
        } else {
          sendTimeBuilder_.setMessage(value);
        }

        return this;
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      public Builder setSendTime(
          com.google.protobuf.Timestamp.Builder builderForValue) {
        if (sendTimeBuilder_ == null) {
          sendTime_ = builderForValue.build();
          onChanged();
        } else {
          sendTimeBuilder_.setMessage(builderForValue.build());
        }

        return this;
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      public Builder mergeSendTime(com.google.protobuf.Timestamp value) {
        if (sendTimeBuilder_ == null) {
          if (sendTime_ != null) {
            sendTime_ =
              com.google.protobuf.Timestamp.newBuilder(sendTime_).mergeFrom(value).buildPartial();
          } else {
            sendTime_ = value;
          }
          onChanged();
        } else {
          sendTimeBuilder_.mergeFrom(value);
        }

        return this;
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      public Builder clearSendTime() {
        if (sendTimeBuilder_ == null) {
          sendTime_ = null;
          onChanged();
        } else {
          sendTime_ = null;
          sendTimeBuilder_ = null;
        }

        return this;
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      public com.google.protobuf.Timestamp.Builder getSendTimeBuilder() {
        
        onChanged();
        return getSendTimeFieldBuilder().getBuilder();
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      public com.google.protobuf.TimestampOrBuilder getSendTimeOrBuilder() {
        if (sendTimeBuilder_ != null) {
          return sendTimeBuilder_.getMessageOrBuilder();
        } else {
          return sendTime_ == null ?
              com.google.protobuf.Timestamp.getDefaultInstance() : sendTime_;
        }
      }
      /**
       * <code>.google.protobuf.Timestamp sendTime = 4;</code>
       */
      private com.google.protobuf.SingleFieldBuilderV3<
          com.google.protobuf.Timestamp, com.google.protobuf.Timestamp.Builder, com.google.protobuf.TimestampOrBuilder> 
          getSendTimeFieldBuilder() {
        if (sendTimeBuilder_ == null) {
          sendTimeBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
              com.google.protobuf.Timestamp, com.google.protobuf.Timestamp.Builder, com.google.protobuf.TimestampOrBuilder>(
                  getSendTime(),
                  getParentForChildren(),
                  isClean());
          sendTime_ = null;
        }
        return sendTimeBuilder_;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:protocol.SocketChanelResponse)
    }

    // @@protoc_insertion_point(class_scope:protocol.SocketChanelResponse)
    private static final com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse();
    }

    public static com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<SocketChanelResponse>
        PARSER = new com.google.protobuf.AbstractParser<SocketChanelResponse>() {
      @java.lang.Override
      public SocketChanelResponse parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new SocketChanelResponse(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<SocketChanelResponse> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<SocketChanelResponse> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto.SocketChanelResponse getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_protocol_SocketChanelResponse_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_protocol_SocketChanelResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\032SocketChanelResponse.proto\022\010protocol\032\037" +
      "google/protobuf/timestamp.proto\"v\n\024Socke" +
      "tChanelResponse\022\022\n\nresponseId\030\002 \001(\003\022\016\n\006r" +
      "esMsg\030\001 \001(\t\022\014\n\004type\030\003 \001(\005\022,\n\010sendTime\030\004 " +
      "\001(\0132\032.google.protobuf.TimestampBN\n1com.h" +
      "ms.socket.socketchanelcommon.common.prot" +
      "ocolB\031SocketChanelResponseProtob\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          com.google.protobuf.TimestampProto.getDescriptor(),
        });
    internal_static_protocol_SocketChanelResponse_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_protocol_SocketChanelResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_protocol_SocketChanelResponse_descriptor,
        new java.lang.String[] { "ResponseId", "ResMsg", "Type", "SendTime", });
    com.google.protobuf.TimestampProto.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
