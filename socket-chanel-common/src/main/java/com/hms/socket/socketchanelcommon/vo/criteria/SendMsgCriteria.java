package com.hms.socket.socketchanelcommon.vo.criteria;


import com.hms.utils.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author hms
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class SendMsgCriteria extends BaseEntity {

    private String msg ;

    private Long userId ;

    private LocalDateTime createTime;

}
