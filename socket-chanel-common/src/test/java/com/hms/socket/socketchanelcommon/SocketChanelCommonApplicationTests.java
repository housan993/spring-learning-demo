package com.hms.socket.socketchanelcommon;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocketChanelCommonApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void proto() throws InvalidProtocolBufferException {
        SocketChanelRequestProto.SocketChanelRequest.Builder builder =
                SocketChanelRequestProto.SocketChanelRequest.newBuilder();
        builder.setRequestId(1)
                .setType(2)
                .setReqMsg("asd");
        System.out.println(builder);
        System.out.println(JsonFormat.printer().print(builder));


        SocketChanelRequestProto.SocketChanelRequest socketChanelRequest =
                SocketChanelRequestProto.SocketChanelRequest.newBuilder()
                        .setRequestId(2)
                        .setType(12)
                        .setReqMsg("3")
                        .build();

        System.out.println(JsonFormat.printer().print(socketChanelRequest));

        SocketChanelRequestProto.SocketChanelRequest protocol =
                SocketChanelRequestProto.SocketChanelRequest.newBuilder()
                .setRequestId(123L)
                .setReqMsg("你好啊")
                .setType(0)
                .build();

        byte[] encode = encode(protocol);

        SocketChanelRequestProto.SocketChanelRequest parseFrom = decode(encode);

        System.out.println("asd="+protocol.toString());
        System.out.println("hms="+protocol.toString().equals(parseFrom.toString()));

    }

    /**
     * 编码
     * @param protocol
     * @return
     */
    public static byte[] encode(SocketChanelRequestProto.SocketChanelRequest protocol){
        return protocol.toByteArray() ;
    }

    /**
     * 解码
     * @param bytes
     * @return
     * @throws InvalidProtocolBufferException
     */
    public static SocketChanelRequestProto.SocketChanelRequest decode(byte[] bytes) throws InvalidProtocolBufferException {
        return SocketChanelRequestProto.SocketChanelRequest.parseFrom(bytes);
    }
}
