package com.hms.memoinfo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

public class Dependency {

    static final int BLOCK = -1, WAIT = 0, SUCCESS = 1;
    static final int UN_KNOW_LEVEL = -1, ROOT_LEVEL = 0;

    public static void main(String[] args) throws JsonProcessingException {
        String
                order = "order", payment = "payment"
                , merchant = "merchant", bidding = "bidding"
                , commodity = "commodity", deposit = "deposit"
                , inv = "inv", user = "user";
        String[][] data = {
                {order, payment, merchant, bidding}
                , {bidding, merchant}
                , {deposit, bidding, merchant, order}
                , {merchant}
                , {payment}
                , {commodity}
                , {inv}
                , {user}
        };

        System.out.println("\nrow data:");
        for (String[] d : data) {
            System.out.format("%s:\t", d[0]);
            for (int i = 1; i < d.length; i++) {
                System.out.format("%s\t", d[i]);
            }
            System.out.println();
        }

        // analyze dependency
        Map<String, Item> map = new HashMap<>();
        int remain = data.length, last = remain;
        System.out.println("\nanalyze start");
        System.out.println("remain: " + remain);
        while (remain > 0) {
            for (String[] d : data)
                if (tryResolve(d, map))
                    remain--;
            System.out.println("remain: " + remain);
            if (remain == last) {
                // todo check circle dependency
                throw new RuntimeException("can not resolve");
            }
            last = remain;
        }
        ObjectMapper json = new ObjectMapper();
        System.out.println("analyze end");
        System.out.println("\nresult:\n" + json.writeValueAsString(map));

        // sort by level
        System.out.println("\nsorted result:");
        List<List<Item>> list = new ArrayList<>();
        map.forEach((name, item) -> {
            while (item.level + 1 > list.size())
                list.add(new ArrayList<>());
            list.get(item.level).add(item);
        });
        sortPrint(list);
        while (true) {
            System.out.println("enter a name(quit: q):");
            Scanner scanner = new Scanner(System.in);
            String cmd = scanner.next();
            if (cmd.toLowerCase().equals("q")) {
                System.out.println("bye");
                break;
            }
            Item item = map.get(cmd);
            if (item == null) {
                System.out.println("%s not exist");
                continue;
            }
            item.status = SUCCESS;
            refreshStatus(map);
            sortPrint(list);
        }
    }

    private static void refreshStatus(Map<String, Item> map) {
        map.forEach((name, item) -> {
            refreshStatus(item);
        });
    }

    private static void refreshStatus(Item item) {
        if (item.status == SUCCESS || item.status == WAIT)
            return;
        for (Map.Entry<String, Item> entry : item.dependency.entrySet())
            if (entry.getValue().status != SUCCESS)
                return;
        item.status = WAIT;
    }

    private static void sortPrint(List<List<Item>> list) {
        for (List<Item> items : list) {
            System.out.format("level %d:\t", items.get(0).level);
            for (Item i : items)
                System.out.format("%s%s\t", formatStatus(i.status), i.name);
            System.out.println();
        }
    }

    private static String formatStatus(int status) {
        switch (status) {
            case WAIT:
                return "*";
            case SUCCESS:
                return "**";
            case BLOCK:
            default:
                return "";
        }
    }

    private static boolean tryResolve(String[] arr, Map<String, Item> map) {
        String name = arr[0];
        Item item = map.get(name);
        if (item == null) {
            item = new Item();
            map.put(name, item);
            item.name = name;
            if (arr.length == 1) {
                item.level = ROOT_LEVEL;
                item.status = WAIT;
                // root init
                return true;
            }
        }
        if (item.level != UN_KNOW_LEVEL) {
            // already resolve
            return false;
        }
        for (int i = 1; i < arr.length; i++) {
            String dName = arr[i];
            if (item.dependency.containsKey(dName))
                continue;
            Item dItem = map.get(dName);
            if (dItem == null)
                continue;
            item.dependency.put(dName, dItem);
        }
        if (item.dependency.size() < arr.length - 1)
            return false;
        int max = UN_KNOW_LEVEL;
        for (Map.Entry<String, Item> e : item.dependency.entrySet()) {
            int current = e.getValue().level;
            if (current == -1)
                return false;
            if (current > max)
                max = current;
        }
        if (max > UN_KNOW_LEVEL) {
            item.level = max + 1;
            return true;
        }
        return false;
    }

    private static class Item {
        public String name;
        public Map<String, Item> dependency = new HashMap<>();
        public int status = BLOCK;
        public int level = UN_KNOW_LEVEL;
    }

}
