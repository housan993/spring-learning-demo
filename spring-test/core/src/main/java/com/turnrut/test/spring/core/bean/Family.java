package com.turnrut.test.spring.core.bean;

import lombok.Data;

@Data
public class Family {
    private Person dad;
    private Person mom;
}
