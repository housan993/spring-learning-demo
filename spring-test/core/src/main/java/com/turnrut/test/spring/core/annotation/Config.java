package com.turnrut.test.spring.core.annotation;

import com.turnrut.test.spring.core.bean.Family;
import com.turnrut.test.spring.core.bean.Person;

@Configuration
@EnableAspectJAutoProxy
@PropertySource("test.properties")
public class Config {

    @Bean
    public Person adam() {
        String name = "adam";
        System.out.printf("init %s\n", name);
        Person person = new Person();
        person.setName(name);
        person.setAge(18);
        person.setGender(1);
        return person;
    }

    @Bean
    public Person eve() {
        String name = "eve";
        System.out.printf("init %s\n", name);
        Person person = new Person();
        person.setName(name);
        person.setAge(17);
        person.setGender(0);
        return person;
    }

    @Bean
    public Family family() {
        System.out.println("init family");
        Family family = new Family();
        family.setMom(eve());
        family.setDad(adam());
        return family;
    }
}
