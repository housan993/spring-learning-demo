package com.turnrut.test.spring.core.service;

import com.turnrut.test.spring.core.bean.Person;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Data
@Service
public class PersonService {
    @Resource
    private Person adam;
    @Resource
    private Person eve;
    @Resource
    PersonService self;
    public void sayName(Person person) {
        System.out.println(person.getName());
    }

    public void adamName() {
        self.sayName(adam);
    }

    public void eveName() {
        self.sayName(eve);
    }
}
