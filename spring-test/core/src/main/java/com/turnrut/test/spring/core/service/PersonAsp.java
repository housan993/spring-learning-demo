package com.turnrut.test.spring.core.service;

import com.turnrut.test.spring.core.bean.Person;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class PersonAsp {

    @Pointcut("execution(* PersonService.sayName(..)) && args(person)")
    public void sayNamePoint(Person person) {}

    @Around("sayNamePoint(person)")
    public Object sayNameAround(ProceedingJoinPoint pjp, Person person) throws Throwable {
        System.out.println("\n\tsayNameAround -- begin");
        System.out.println("\t" + person);
        Object retValue = pjp.proceed();
        System.out.println("\tsayNameAround -- end");
        return retValue;
    }

}
