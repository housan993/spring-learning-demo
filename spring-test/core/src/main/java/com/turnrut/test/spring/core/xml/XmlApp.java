package com.turnrut.test.spring.core.xml;

import com.turnrut.test.spring.core.bean.Family;
import com.turnrut.test.spring.core.bean.Person;
import com.turnrut.test.spring.core.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class XmlApp {
    public static void main(String[] args) {
        log.error("111");
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        PersonService personService = context.getBean(PersonService.class);
        Person person = context.getBean("adam", Person.class);
        Family family = context.getBean(Family.class);
        System.out.println(family);
        personService.sayName(person);
        personService.eveName();
        personService.adamName();
    }

}
