package com.turnrut.test.spring.core.bean;

import lombok.Data;

@Data
public class Person {
    private String name;
    private Integer age;
    private Integer gender; // 0 女,1 男
}
