package com.turnrut.test.spring.core.annotation;

import com.turnrut.test.spring.core.bean.Family;
import com.turnrut.test.spring.core.bean.Person;
import com.turnrut.test.spring.core.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AnnoApp {
    @Resource
    private Person adam;
    @Autowired
    private Family family;
    @Autowired
    private PersonService personService;
    @Value("${ultimate.question}")
    String question;
    @Value("${ultimate.answer}")
    Integer answer;
    @Value("${ultimate.answer}")
    String answerStr;
    @Value("#{adam.name}")
    String adamName;

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("com.turnrut.test.spring.core");
        context.refresh();
        AnnoApp app = context.getBean(AnnoApp.class);
        app.run();
    }

    void run() {
        System.out.printf("family: %s\n", family.toString());
        System.out.printf("question.length: %s\n", question.length());
        System.out.printf("question: %s\n", question);
        System.out.printf("answer: %d\n", answer);
        System.out.printf("answerStr: %s\n", answerStr);
        System.out.printf("adamName: %s\n", adamName);
        personService.sayName(adam);
        personService.eveName();
        personService.adamName();
    }
}
