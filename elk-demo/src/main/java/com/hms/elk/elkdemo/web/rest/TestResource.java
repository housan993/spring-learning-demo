package com.hms.elk.elkdemo.web.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hms
 * @date 2018/12/26 14:28
 */

@RestController
@RequestMapping("elk")
public class TestResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final Logger apmInfoLogger = LoggerFactory.getLogger("APMInfoDev");
    private final Logger hmsElkLog = LoggerFactory.getLogger("HMS_ELK_LOG");

    @RequestMapping("/log")
    public String log() {
        log.debug("这是debug");
        log.info("这是info");
        log.error("这是error");

        apmInfoLogger.debug("APMInfoDev-debug");
        apmInfoLogger.info("APMInfoDev-info");
        apmInfoLogger.error("APMInfoDev-error");

        hmsElkLog.debug("hmsElkLog-debug");
        hmsElkLog.info("hmsElkLog-info");
        hmsElkLog.error("hmsElkLog-error");

        return "";
    }

}
