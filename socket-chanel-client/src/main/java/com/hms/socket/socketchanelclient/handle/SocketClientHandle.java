package com.hms.socket.socketchanelclient.handle;

import com.hms.socket.socketchanelclient.utils.SpringBeanFactory;
import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelResponseProto;
import com.hms.socket.socketchanelcommon.constant.Constants;
import com.hms.socket.socketchanelcommon.utils.NettyUtils;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author hms
 */
@ChannelHandler.Sharable
public class SocketClientHandle extends SimpleChannelInboundHandler<SocketChanelResponseProto.SocketChanelResponse> {

    private final static Logger log = LoggerFactory.getLogger(SocketClientHandle.class);

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        log.info("定时检测服务端是否存活");
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;

            log.info("定时检测服务端是否空闲");

            if (idleStateEvent.state() == IdleState.WRITER_IDLE) {
                SocketChanelRequestProto.SocketChanelRequest heartBeat =
                        SpringBeanFactory.getBean("heartBeat", SocketChanelRequestProto.SocketChanelRequest.class);
                ctx.writeAndFlush(heartBeat).addListeners((ChannelFutureListener) future -> {
                    if (!future.isSuccess()) {
                        log.error("IO error,close Channel");
                        future.channel().close();
                    }
                });
            }

        }

        super.userEventTriggered(ctx, evt);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        //客户端和服务端建立连接时调用
        log.info("socket server connect success!");
    }

    //    @Autowired SocketClient socketClient;
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.info("客户端断开了，重新连接！");


//        if (scheduledExecutorService == null){
//            scheduledExecutorService = SpringBeanFactory.getBean("scheduledTask",ScheduledExecutorService.class) ;
//        }
//        log.info("客户端断开了，重新连接！");
//        // TODO: 2019-01-22 后期可以改为不用定时任务，连上后就关闭任务 节省性能。
//        scheduledExecutorService.scheduleAtFixedRate(new ReConnectJob(ctx),0,10, TimeUnit.SECONDS) ;
//        socketClient.reconnect();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, SocketChanelResponseProto.SocketChanelResponse msg) throws Exception {
        log.info("客户端收到消息msg={}", msg.toString());
        //心跳更新时间
        if (msg.getType() == Constants.CommandType.PING) {
            //log.info("收到服务端心跳！！！");
            NettyUtils.updateReaderTime(ctx.channel(), System.currentTimeMillis());
        }

        if (msg.getType() != Constants.CommandType.PING) {
            //回调消息
            log.error("消息回调");
//            callBackMsg(msg.getResMsg());

            //将消息中的 emoji 表情格式化为 Unicode 编码以便在终端可以显示
//            String response = EmojiParser.parseToUnicode(msg.getResMsg());
//            System.out.println(response);
        }


    }

//    /**
//     * 回调消息
//     * @param msg
//     */
//    private void callBackMsg(String msg) {
//        threadPoolExecutor = SpringBeanFactory.getBean("callBackThreadPool",ThreadPoolExecutor.class) ;
//        threadPoolExecutor.execute(() -> {
//            caller = SpringBeanFactory.getBean(MsgHandleCaller.class) ;
//            caller.getMsgHandleListener().handle(msg);
//        });
//
//    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //异常时断开连接
        cause.printStackTrace();
        ctx.close();
    }
}
