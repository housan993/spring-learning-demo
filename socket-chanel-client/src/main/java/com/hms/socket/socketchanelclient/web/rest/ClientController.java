package com.hms.socket.socketchanelclient.web.rest;


import com.hms.socket.socketchanelclient.client.SocketClient;
import com.hms.socket.socketchanelcommon.vo.criteria.GoogleProtocolCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author hms
 */
@RestController
@RequestMapping("/")
public class ClientController {

    private final static Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private SocketClient socketClient;

    /**
     * 向服务端发消息 Google ProtoBuf
     *
     * @param googleProtocolCriteria
     * @return
     */
    @RequestMapping(value = "sendProtoBufMsg", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> sendProtoBufMsg(@RequestBody GoogleProtocolCriteria googleProtocolCriteria) {

        try {
            log.info("入参={}", googleProtocolCriteria);
            socketClient.start(googleProtocolCriteria);

            socketClient.sendGoogleProtocolMsg(googleProtocolCriteria);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
