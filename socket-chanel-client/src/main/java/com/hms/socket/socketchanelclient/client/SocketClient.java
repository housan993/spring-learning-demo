package com.hms.socket.socketchanelclient.client;

import com.hms.socket.socketchanelclient.init.SocketClientInitializer;
import com.hms.socket.socketchanelclient.service.RouteService;
import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import com.hms.socket.socketchanelcommon.constant.Constants;
import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import com.hms.socket.socketchanelcommon.entity.SocketUsrInfo;
import com.hms.socket.socketchanelcommon.utils.SessionSocketUtils;
import com.hms.socket.socketchanelcommon.vo.criteria.GoogleProtocolCriteria;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.DefaultThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hms
 */
@Component
public class SocketClient {

    private final static Logger log = LoggerFactory.getLogger(SocketClient.class);

    private EventLoopGroup group = new NioEventLoopGroup(0, new DefaultThreadFactory("client-work"));

    @Resource
    private RouteService routeService;

    private ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>(16);

    /**
     * 重试次数
     */
    private int errorCount;

    public void start(GoogleProtocolCriteria googleProtocolCriteria) {

        String key = "uni" + "_" + googleProtocolCriteria.getTrackId() + "_" + googleProtocolCriteria.getUserId();

        if (null == concurrentHashMap.putIfAbsent(key, key)) {
            try {
                //启动客户端
                startClient(googleProtocolCriteria);
            } finally {
                concurrentHashMap.remove(key);
            }
        } else {
            log.info("用户并发连接服务器中={}", googleProtocolCriteria);
        }
    }

    /**
     * 启动客户端
     *
     * @param googleProtocolCriteria param
     */
    private void startClient(GoogleProtocolCriteria googleProtocolCriteria) {
        Long userId = googleProtocolCriteria.getUserId();

        NioSocketChannel nioSocketChannel = SessionSocketUtils.get(userId);

        if (null != nioSocketChannel) {
            log.info("客户[{}]已经连接", userId);
            return;
        }

        SocketServerInfo serverInfo = routeService.distribution(userId);

        if (null == serverInfo) {
            log.error("客户[{}]获取服务器信息失败", userId);
            return;
        }

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .handler(new SocketClientInitializer())
        ;

        ChannelFuture future;
        try {
            future = bootstrap.connect(serverInfo.getServerIp(), serverInfo.getServerNettyPort()).sync();
            if (future.isSuccess()) {
                log.info("start cim client success!");
                log.info("启动 cim client 成功");

                NioSocketChannel channel = (NioSocketChannel) future.channel();
                SessionSocketUtils.put(userId, channel);

                //向服务端注册
                loginSocketServer(channel, userId, googleProtocolCriteria.getUserName());
                return;
            }
            log.info("启动 socket client 异常={}", future);
            log.info("失败次数={}", errorCount);
        } catch (InterruptedException e) {
            errorCount++;

//            if (errorCount >= configuration.getErrorCount()) {
//                log.error("连接失败次数达到上限[{}]次", errorCount);
//                msgHandle.shutdown();
//            }
            log.error("连接失败", e);
        }
    }

    /**
     * 向服务器注册
     */
    private void loginSocketServer(NioSocketChannel nioSocketChannel, Long userId, String userName) {
        SocketChanelRequestProto.SocketChanelRequest login = SocketChanelRequestProto
                .SocketChanelRequest.newBuilder()
                .setRequestId(userId)
                .setReqMsg(userName)
                .setType(Constants.CommandType.LOGIN)
                .build();
        ChannelFuture future = nioSocketChannel.writeAndFlush(login);
        future.addListener((ChannelFutureListener) channelFuture ->
                log.info("registry cim server success!")
        );
    }

    /**
     * 发送 Google Protocol 编解码字符串
     *
     * @param googleProtocolCriteria
     */
    public void sendGoogleProtocolMsg(GoogleProtocolCriteria googleProtocolCriteria) {
        NioSocketChannel nioSocketChannel = SessionSocketUtils.get(googleProtocolCriteria.getUserId());

        SocketChanelRequestProto.SocketChanelRequest protocol = SocketChanelRequestProto.SocketChanelRequest
                .newBuilder()
                .setRequestId(googleProtocolCriteria.getRequestId())
                .setReqMsg(googleProtocolCriteria.getMsg())
                .setType(Constants.CommandType.MSG)
                .build();

        ChannelFuture future = nioSocketChannel.writeAndFlush(protocol);
        future.addListener((ChannelFutureListener) channelFuture ->
                log.info("客户端手动发送 Google Protocol 成功={}", googleProtocolCriteria.toString()));

    }


    public void reconnect(SocketServerInfo socketServerInfo, SocketUsrInfo socketUsrInfo) throws Exception {

        NioSocketChannel nioSocketChannel = SessionSocketUtils.get(socketUsrInfo.getUserId());
        if (nioSocketChannel != null && nioSocketChannel.isActive()) {
            return;
        }
        //首先清除路由信息，下线

        GoogleProtocolCriteria googleProtocolCriteria = new GoogleProtocolCriteria();
        log.info("reconnect....");
        start(googleProtocolCriteria);
        log.info("reconnect success");
    }

    /**
     * 关闭
     *
     * @throws InterruptedException
     */
    public void close(SocketUsrInfo socketUsrInfo) throws InterruptedException {
        NioSocketChannel nioSocketChannel = SessionSocketUtils.get(socketUsrInfo.getUserId());
        if (nioSocketChannel != null) {
            nioSocketChannel.close();
        }
    }
}
