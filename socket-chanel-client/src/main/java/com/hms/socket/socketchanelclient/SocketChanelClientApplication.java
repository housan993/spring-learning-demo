package com.hms.socket.socketchanelclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketChanelClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocketChanelClientApplication.class, args);
    }

}
