package com.hms.socket.socketchanelclient.service;

import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import io.netty.channel.Channel;

/**
 * @author hms
 */
public interface RouteService {

    /**
     * 获取服务节点信息
     * @param userId
     * @return
     */
    SocketServerInfo distribution(Long userId);

}
