package com.hms.socket.socketchanelclient.config;

import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import com.hms.socket.socketchanelcommon.constant.Constants;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author hms
 */
@Configuration
public class BeanConfig {


    /**
     * http client
     * @return okHttp
     */
    @Bean
    public OkHttpClient okHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10,TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);
        return builder.build();
    }


    /**
     * 创建心跳单例
     * @return
     */
    @Bean(value = "heartBeat")
    public SocketChanelRequestProto.SocketChanelRequest heartBeat() {
        return SocketChanelRequestProto
                .SocketChanelRequest.newBuilder()
                .setRequestId(0L)
                .setReqMsg("pong")
                .setType(Constants.CommandType.PING)
                .build();
    }
}
