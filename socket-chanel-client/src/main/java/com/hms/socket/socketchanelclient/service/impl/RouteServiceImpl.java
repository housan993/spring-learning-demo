package com.hms.socket.socketchanelclient.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hms.socket.socketchanelclient.service.RouteService;
import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import com.hms.utils.base.BaseView;
import com.hms.utils.utils.OkHttpUtils;
import io.netty.channel.Channel;
import io.netty.channel.socket.nio.NioSocketChannel;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author hms
 */
@Service
public class RouteServiceImpl implements RouteService {

    private final static Logger log = LoggerFactory.getLogger(RouteServiceImpl.class);

    @Resource
    private OkHttpClient okHttpClient;

    @Value("${socket-channel.server.url}")
    private String serverUrl;

    @Value("${socket-channel.route.url}")
    private String routeUrl;

    @Override
    public SocketServerInfo distribution(Long userId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", userId);

            String url = routeUrl + "/route/distribution";
            String result = OkHttpUtils.post(url, jsonObject, okHttpClient);

            BaseView baseView = JSON.parseObject(result, BaseView.class);
            return JSON.parseObject(JSON.toJSONString(baseView.getData()), SocketServerInfo.class);
        } catch (Exception e) {
            log.error("异常", e);
        }
        return null;

    }
}
