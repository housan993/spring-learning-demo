package com.hms.socket.socketchanel.web.rest;

import com.google.common.collect.Maps;
import com.hms.socket.socketchanel.server.SocketServer;
import com.hms.socket.socketchanel.service.RouteService;
import com.hms.socket.socketchanelcommon.vo.criteria.SendMsgCriteria;
import com.hms.utils.base.BaseController;
import com.hms.utils.base.BaseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author hms
 */
@RestController
@RequestMapping("route")
public class RouteRest extends BaseController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private SocketServer socketServer;

    @Resource
    private RouteService routeService;

    /**
     * 向服务端发消息
     *
     * @param sendMsgCriteria
     * @return
     */
    @PostMapping("/server/send")
    @ResponseBody
    public ResponseEntity<BaseView> sendMsg(@RequestBody SendMsgCriteria sendMsgCriteria) {
        log.info("入参:sendMsgCriteria={}", sendMsgCriteria);
        socketServer.sendMsg(sendMsgCriteria);
        BaseView baseView = new BaseView("发送成功");
        return new ResponseEntity<>(baseView, HttpStatus.OK);
    }

    @GetMapping("/info")
    public ResponseEntity<BaseView> getUserInfo(HttpServletRequest request) {
        Map<String, Object> result = Maps.newHashMap();
        List<String> list = routeService.serverAddress();
        log.info("list={}", list);
        BaseView baseView = new BaseView("查询成功", result);
        return new ResponseEntity<>(baseView, HttpStatus.OK);
    }

    @PostMapping("/logout")
    public ResponseEntity<BaseView> logout() {
        Map<String, Object> result = Maps.newHashMap();
        BaseView baseView = new BaseView("退出成功", result);
        return new ResponseEntity<>(baseView, HttpStatus.OK);
    }

}
