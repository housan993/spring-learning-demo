package com.hms.socket.socketchanel.vo.criteria;


import com.hms.utils.base.BaseEntity;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author hms
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class SessionPutCriteria extends BaseEntity {
    private Long userId;
    private NioSocketChannel channel;

}
