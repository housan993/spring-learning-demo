package com.hms.socket.socketchanel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketChanelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocketChanelApplication.class, args);
    }

}
