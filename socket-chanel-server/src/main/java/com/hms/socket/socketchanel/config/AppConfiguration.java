package com.hms.socket.socketchanel.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author hms
 */
@Component
public class AppConfiguration {

    @Value("${socket-channel.heartbeat.time}")
    private long heartBeatTime ;

    public long getHeartBeatTime() {
        return heartBeatTime;
    }

    public void setHeartBeatTime(long heartBeatTime) {
        this.heartBeatTime = heartBeatTime;
    }
}
