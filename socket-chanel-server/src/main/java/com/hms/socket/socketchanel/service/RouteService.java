package com.hms.socket.socketchanel.service;


import io.netty.channel.Channel;

import java.util.List;

/**
 *
 * @author hms
 */
public interface RouteService {

    /**
     * 服务端地址信息
     * @return
     */
    List<String> serverAddress();

    void addAddress(String address);

    void addAddress(String ip, int port, int nettyPort);
}
