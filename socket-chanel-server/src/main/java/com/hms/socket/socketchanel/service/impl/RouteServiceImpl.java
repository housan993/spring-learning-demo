package com.hms.socket.socketchanel.service.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.hms.socket.socketchanel.service.RouteService;
import com.hms.socket.socketchanelcommon.enums.SymbolEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

/**
 * @author hms
 */
@Service
public class RouteServiceImpl implements RouteService {

    private final static Logger log = LoggerFactory.getLogger(RouteServiceImpl.class);

    @Value("${socket-channel.netty.port}")
    private String port;

    @Override
    public List<String> serverAddress() {
        //获得本机IP
        List<String> list = Lists.newArrayList();
        try {
            String addr = InetAddress.getLocalHost().getHostAddress();
            log.info("本机地址={}", addr);

            if (StringUtils.isBlank(port)) {
                log.error("请添加服务端口");
                return list;
            }
            if (StringUtils.contains(port, SymbolEnum.COMMA.getSymbol())) {
                List<String> ports = Splitter.on(",").trimResults().splitToList(port);
                ports.forEach(tmpPort -> list.add(addr + "-" + tmpPort));
            } else {
                list.add(addr + "-" + port);
            }
            return list;
        } catch (UnknownHostException e) {
            log.error("获取主机地址失败", e);
        }
        return list;
    }

    @Override
    public void addAddress(String address) {

    }

    @Override
    public void addAddress(String ip, int port, int nettyPort) {

    }
}
