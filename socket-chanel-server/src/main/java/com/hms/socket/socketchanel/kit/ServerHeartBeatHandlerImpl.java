package com.hms.socket.socketchanel.kit;

import com.hms.socket.socketchanel.config.AppConfiguration;
import com.hms.socket.socketchanelcommon.entity.SocketUsrInfo;
import com.hms.socket.socketchanelcommon.utils.NettyUtils;
import com.hms.socket.socketchanelcommon.utils.SessionSocketUtils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hms
 */
@Service
public class ServerHeartBeatHandlerImpl implements HeartBeatHandler {

    private final static Logger log = LoggerFactory.getLogger(ServerHeartBeatHandlerImpl.class);

    @Autowired
    private AppConfiguration appConfiguration;

    @Override
    public void process(ChannelHandlerContext ctx) throws Exception {

        long heartBeatTime = appConfiguration.getHeartBeatTime() * 1000;

        Long lastReadTime = NettyUtils.getReaderTime(ctx.channel());
        long now = System.currentTimeMillis();
        if (lastReadTime != null && now - lastReadTime > heartBeatTime) {
            SocketUsrInfo userInfo = SessionSocketUtils.getUserId((NioSocketChannel) ctx.channel());
            if (userInfo != null) {
                log.warn("客户端[{}]心跳超时[{}]ms，需要关闭连接!", userInfo.getUserName(), now - lastReadTime);
            }
            assert userInfo != null;
            log.info("用户[{}]下线", userInfo.getUserName());
            SessionSocketUtils.removeSession(userInfo.getUserId());
            SessionSocketUtils.remove((NioSocketChannel) ctx.channel());
            ctx.channel().close();
        }
    }
}
