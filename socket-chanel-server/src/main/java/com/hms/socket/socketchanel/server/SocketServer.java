package com.hms.socket.socketchanel.server;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hms.socket.socketchanel.init.SocketServerInitializer;
import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import com.hms.socket.socketchanelcommon.constant.Constants;
import com.hms.socket.socketchanelcommon.utils.SessionSocketUtils;
import com.hms.socket.socketchanelcommon.vo.criteria.SendMsgCriteria;
import com.hms.utils.base.BaseView;
import com.hms.utils.utils.OkHttpUtils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;


/**
 * @author hms
 */
@Component
public class SocketServer {

    private final static Logger log = LoggerFactory.getLogger(SocketServer.class);

    private EventLoopGroup boss = new NioEventLoopGroup();
    private EventLoopGroup work = new NioEventLoopGroup();

    @Resource
    private OkHttpClient okHttpClient;

    @Value("${server.port}")
    private int port;

    @Value("${socket-channel.netty.port}")
    private int nettyPort;

    @Value("${socket-channel.route.url}")
    private String routeUrl;

    /**
     * 启动 sc server
     *
     * @return
     * @throws InterruptedException
     */
    @PostConstruct
    public void start() throws InterruptedException, UnknownHostException {
        ServerBootstrap bootstrap = new ServerBootstrap()
                .group(boss, work)
                .channel(NioServerSocketChannel.class)
                .localAddress(new InetSocketAddress(nettyPort))
                //保持长连接
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childHandler(new SocketServerInitializer());

        ChannelFuture future = bootstrap.bind().sync();
        if (future.isSuccess()) {
            // hms 服务器注册到路由
            nodeRegister();
            log.info("启动 socket channel server 成功");
        }
    }

    private void nodeRegister() throws UnknownHostException {
        String serverIp = InetAddress.getLocalHost().getHostAddress();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("serverIp", serverIp);
        jsonObject.put("serverNettyPort", nettyPort);
        jsonObject.put("serverHttpPort", port);

        String url = routeUrl + "/route/node-register";
        String result = OkHttpUtils.post(url, jsonObject, okHttpClient);

        BaseView baseView = JSON.parseObject(result, BaseView.class);
        if (StringUtils.isBlank(baseView.getMsg())) {
            throw new RuntimeException("服务器节点[" + serverIp + "]注册失败");
        }
    }

    /**
     * 销毁
     */
    @PreDestroy
    public void destroy() {
        boss.shutdownGracefully().syncUninterruptibly();
        work.shutdownGracefully().syncUninterruptibly();
        log.info("关闭 socket channel server 成功");
    }

    /**
     * 发送 Google Protocol 编码消息
     *
     * @param sendMsgCriteria 消息
     */
    public void sendMsg(SendMsgCriteria sendMsgCriteria) {
        NioSocketChannel socketChannel = SessionSocketUtils.get(sendMsgCriteria.getUserId());

        if (null == socketChannel) {
            throw new NullPointerException("客户端[" + sendMsgCriteria.getUserId() + "]不在线！");
        }
        SocketChanelRequestProto.SocketChanelRequest protocol = SocketChanelRequestProto
                .SocketChanelRequest.newBuilder()
                .setRequestId(sendMsgCriteria.getUserId())
                .setReqMsg(sendMsgCriteria.getMsg())
                .setType(Constants.CommandType.MSG)
                .build();

        ChannelFuture future = socketChannel.writeAndFlush(protocol);
        future.addListener((ChannelFutureListener) channelFuture ->
                log.info("服务端手动发送 Google Protocol 成功={}", sendMsgCriteria.toString()));
    }
}
