package com.hms.jvm.bugdemo.web.rest;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author hms
 * @date 2018/12/26 14:28
 */

@Api(tags = {"jvm-bug"})
@RestController
@RequestMapping("/bug")
public class BugRest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final int MEN_1M = 1024 * 1024;

    @ApiOperation("构建一个堆栈内存")
    @PostMapping("/build")
    public ResponseEntity<String> build(@RequestBody Map<String, Object> res) {
        log.info("res={}", res);
        int num = (Integer) res.get("num");

        for (int i = 0; i < num; i++) {
            byte[] men = new byte[num * MEN_1M];

        }
        return new ResponseEntity<>("", HttpStatus.OK);
    }

}
