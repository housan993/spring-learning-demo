package com.hms.jvm.bugdemo.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Num {
    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(1);
        int totalNum = 33;
        for (int i = 0; i < totalNum; i++) {
            SynRunThread synRunThread = new SynRunThread(i, latch);
            synRunThread.start();
        }
        //计数器減一  所有线程释放 并发访问。
        latch.countDown();
        CountDownLatch latch1 = new CountDownLatch(1);
        int totalNum1 = 36;
        for (int i = 33; i < 33 + totalNum1; i++) {
            SynRunThread synRunThread = new SynRunThread(i, latch1);
            synRunThread.start();
        }
        try {
            TimeUnit.MILLISECONDS.sleep(4998);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //计数器減一  所有线程释放 并发访问。
        latch1.countDown();
    }
}

class SynRunThread extends Thread {
    private CountDownLatch latch;
    private int i;

    SynRunThread(int i, CountDownLatch latch) {
        super();
        this.i = i;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            // 一直阻塞当前线程，直到计时器的值为0
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int aaa = GeneratorNum.next();
        System.out.println(Thread.currentThread().getName() + " == " + i + " == " + aaa);
    }


}

class GeneratorNum {
    private static AtomicInteger start = new AtomicInteger();
    private static AtomicInteger max = new AtomicInteger();
    private static AtomicInteger tmpNum = new AtomicInteger();
    private static final byte[] LOCK = new byte[0];
    private static final Object lock = new Object();
    private static volatile boolean initialized = false;

    private static int sqlNum() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + "获取数据......5s");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(Thread.currentThread().getName() + "获取数据......4s");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(Thread.currentThread().getName() + "获取数据......3s");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(Thread.currentThread().getName() + "获取数据......2s");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(Thread.currentThread().getName() + "获取数据......1s");
        TimeUnit.SECONDS.sleep(1);
        return tmpNum.incrementAndGet();
    }

    private static void refresh() {
        int newNum = 0;
        int step = 1000;
        try {
            newNum = sqlNum();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        start.set((newNum - 1) * step);
        max.set(newNum * step);
    }

    static int next() {
        synchronized (lock) {
            start.incrementAndGet();
            if (start.get() > max.get()) {
                refresh();
                return next();
            } else {
                return start.get();
            }
        }
    }
}