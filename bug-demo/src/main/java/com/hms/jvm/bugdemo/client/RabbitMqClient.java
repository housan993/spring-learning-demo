package com.hms.jvm.bugdemo.client;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 模拟核心mq不消费情况
 *
 * @author hms
 */
public class RabbitMqClient {


    //定义交换器的名字
    private static final String EXCHANGE_NAME = "direct_logs";
    private static final String queueName = "hms";

    static ConnectionFactory factory;
    static Connection connection;
    static Channel channel;

    /**
     * vm参数:
     * -verbose:gc
     * -Xms20M
     * -Xmx20M
     * -Xmn10M
     * -XX:+PrintGC
     * -XX:+PrintGCDetails
     * -XX:SurvivorRatio=8
     * -XX:PretenureSizeThreshold=3145728
     * -XX:+HeapDumpOnOutOfMemoryError
     * -XX:HeapDumpPath=/home/hms/Downloads/idea/java
     */
    public static void connect() {
        try {
            //1.创建一个连接工厂
            factory = new ConnectionFactory();
            //设置要连接的RabbitMQ服务器的地址
            factory.setHost("localhost");
            factory.setPort(5672);
            factory.setVirtualHost("/");
            factory.setUsername("hms");
            factory.setPassword("hms");

            //2.通过连接工厂创建一个连接
            connection = factory.newConnection();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void connectChannel(Connection connection) {
        try {
            System.out.println("连接");
            //3.通过连接创建一个信道 信道是用来传送数据的
            channel = connection.createChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void custom(Channel channel) {
        try {
            System.out.println("消费");
            channel.queueDeclare(queueName, true, false, false, null);

            //4.通过信道声明一个交换器 第一个参数时交换器的名字 第二个参数时交换器的种类
            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);

            //5.声明随机队列
//            String queueName = channel.queueDeclare().getQueue();

            //6.声明一个只消费错误日志的路由键error
            String routingKey = "error";

            //7.队列通过路由键绑定到交换器上
            channel.queueBind(queueName, EXCHANGE_NAME, routingKey);

            System.out.println("Waiting message.......");

            //8.设置一个监听器监听消费消息
            Consumer consumerB = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                                           AMQP.BasicProperties properties, byte[] body)
                        throws IOException {
                    System.out.println("当前线程名" + Thread.currentThread().getName());
                    String message = new String(body, "UTF-8");
                    System.out.println("Accept:" + envelope.getRoutingKey() + ":" + message);
                }
            };
            //9.自动确认：autoAck参数为true
            channel.basicConsume(queueName, true, consumerB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        connect();
        connectChannel(connection);

        for (int i = 0; i < 5; i++) {
            System.out.println("开始" + i);
            custom(channel);
            TimeUnit.SECONDS.sleep(20);
        }
    }
}
