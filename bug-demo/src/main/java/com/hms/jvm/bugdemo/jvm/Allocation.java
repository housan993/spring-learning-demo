package com.hms.jvm.bugdemo.jvm;

/**
 * @author hms
 */
public class Allocation {
    private static final int M_1MB = 1024 * 1024;

    /**
     * vm参数:
     * -verbose:gc
     * -Xms20M
     * -Xmx20M
     * -Xmn10M
     * -XX:+PrintGC
     * -XX:+PrintGCDetails
     * -XX:SurvivorRatio=8
     * -XX:PretenureSizeThreshold=3145728
     * -XX:+HeapDumpOnOutOfMemoryError
     * -XX:HeapDumpPath=/home/hms/Downloads/idea/java
     */
    public static void testAllocation() {
        byte[] allocation1;
        byte[] allocation2;
        byte[] allocation3;
        byte[] allocation4;

        allocation1 = new byte[2 * M_1MB];
        allocation2 = new byte[2 * M_1MB];
        allocation3 = new byte[2 * M_1MB];
        // 出现一次 Minor GC
        allocation4 = new byte[4 * M_1MB];
    }


    /**
     * vm参数:
     * -verbose:gc
     * -Xms20M
     * -Xmx20M
     * -Xmn10M
     * -XX:+PrintGC
     * -XX:+PrintGCDetails
     * -XX:SurvivorRatio=8
     * -XX:+UseSerialGC
     * -XX:PretenureSizeThreshold=3145728
     * -XX:+HeapDumpOnOutOfMemoryError
     * -XX:HeapDumpPath=/home/hms/Downloads/idea/java
     */
    public static void testPretenureSizeThreshold() {
        byte[] allocation1;
        allocation1 = new byte[4 * M_1MB];
    }

    public static void main(String[] args) {
        testPretenureSizeThreshold();
    }
}
