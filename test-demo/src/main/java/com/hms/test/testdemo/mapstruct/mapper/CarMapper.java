package com.hms.test.testdemo.mapstruct.mapper;

/**
 * @author hms
 * @date 2018/12/3 17:50
 */

import com.hms.test.testdemo.mapstruct.Car;
import com.hms.test.testdemo.mapstruct.CarDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarMapper {

    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    @Mapping(source = "numberOfSeats", target = "seatCount")
    CarDto carToCarDto(Car car);
}

