package com.hms.test.testdemo.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Test002 {

    public static void main(String[] args) throws InterruptedException {
        int count = 3;
        CountDownLatch latch1 = new CountDownLatch(1);
        CountDownLatch latch = new CountDownLatch(count);

        List<Thread> threadList = new ArrayList<Thread>();

        for (int i = 0; i < count; i++) {
            threadList.add(new MyThread("Thread-" + i, latch));
        }

        for (Thread thread : threadList) {
            thread.setDaemon(true);
            thread.start();
            latch.countDown();
        }
//        latch1.await();
        System.out.println("asd");
    }

    static class MyThread extends Thread {
        private final CountDownLatch latch;

        MyThread(String name, CountDownLatch latch) {
            super(name);
            this.latch = latch;
        }

        @Override
        public void run() {
            try {
                System.out.println(getName() + " started");
                TimeUnit.SECONDS.sleep(new Random(1000).nextInt());
                latch.await();
                System.out.println(getName() + " finished");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
