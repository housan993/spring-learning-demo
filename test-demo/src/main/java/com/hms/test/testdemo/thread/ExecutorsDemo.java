package com.hms.test.testdemo.thread;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecutorsDemo {

    /**
     * 最终定稿线程池配置
     * 参考:https://blog.csdn.net/wzy_1988/article/details/38922449
     */
    public ExecutorService hmsExecutorService = new ThreadPoolExecutor(3, Integer.MAX_VALUE,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(1024), namedThreadFactory,
            new ThreadPoolExecutor.AbortPolicy());

    private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
            .setNameFormat("demo-pool-%d").build();






    private static ExecutorService pool = new ThreadPoolExecutor(5, 10,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(16), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    public ExecutorService fixedExecutorService = Executors.newFixedThreadPool(5);
    public ExecutorService cachedExecutorService = Executors.newCachedThreadPool();
    public ExecutorService singleExecutorService = Executors.newSingleThreadExecutor();
    public ExecutorService customerExecutorService = new ThreadPoolExecutor(3, 5,
            0, TimeUnit.MILLISECONDS, new SynchronousQueue<Runnable>());

    public void testExecutorException() {
        for (int i = 0; i < 10; i ++) {
            // 增加isShutdown()判断
            if (!fixedExecutorService.isShutdown()) {
                fixedExecutorService.execute(new SayHelloRunnable());
            }
            fixedExecutorService.shutdown();
        }
    }

    public void testCustomerExecutorException() {
        for (int i = 0; i < 100; i ++) {
            customerExecutorService.execute(new SayHelloRunnable());
        }
    }

    private class SayHelloRunnable implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.println("hello world!");
            }

        }
    }

    public static void main(String[] args) {
        ExecutorsDemo executorsDemo = new ExecutorsDemo();
        executorsDemo.testCustomerExecutorException();;
    }

    public static void main1(String[] args) {
        try {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
    //        for (int i = 0; i < 1; i++) {
                Task task = new Task();
                FutureTask<Integer> futureTask = new FutureTask<Integer>(task);
                pool.execute(futureTask);
                System.out.println("线程" + i + " === " + pool.isShutdown());
                try {
    //            System.out.println("线程" + i);
                } catch (Exception e) {
    //                e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(" === " + pool.isShutdown());
//        pool.shutdown();
    }
    static class Task implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
//            System.out.println("子线程在进行计算");
            Thread.sleep(1000);
            System.out.println("子线程结束");
//            int sum = 0;
//            for (int i = 0; i < 100; i++) {
//                sum += i;
//            }
//            return sum;
            return 0;
        }
    }
}
