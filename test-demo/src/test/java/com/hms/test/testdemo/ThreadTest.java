package com.hms.test.testdemo;

import com.google.common.util.concurrent.RateLimiter;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ThreadTest {
    /**
     * 测试限流 高并发
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        Thread[] ts = new Thread[100];
        for (int k = 0; k < 100; k++) {
            ts[k] = new Thread(new accessThread(k));
        }
        //启动线程
        for (int k = 0; k < 100; k++) {
            ts[k].start();
        }

        for (int k = 0; k < 100; k++) {
            ts[k].join();
        }

        System.out.println(i);//输出结果:100000
        System.out.println(j);
    }

    /**
     * 测试重复 高并发
     * @param args
     * @throws InterruptedException
     */
    public static void main2(String[] args) throws InterruptedException {
        Thread[] ts = new Thread[100];
        for (int k = 0; k < 100; k++) {
            ts[k] = new Thread(new lockKeyThread());
        }
        //启动线程
        for (int k = 0; k < 100; k++) {
            ts[k].start();
        }

        for (int k = 0; k < 100; k++) {
            ts[k].join();
        }

        System.out.println(i);//输出结果:100000
        System.out.println(j);
    }

    /**
     * 测试原子类 高并发
     * @param args
     * @throws InterruptedException
     */
    public static void main1(String[] args) throws InterruptedException {
        Thread[] ts = new Thread[10];
        //开启10条线程同时执行i的自增操作
        for (int k = 0; k < 10; k++) {
            ts[k] = new Thread(new AddThread());
        }
        //启动线程
        for (int k = 0; k < 10; k++) {
            ts[k].start();
        }

        for (int k = 0; k < 10; k++) {
            ts[k].join();
        }

        System.out.println(i);//输出结果:100000
    }

    //创建AtomicInteger,用于自增操作
    private static AtomicInteger i = new AtomicInteger();
    private static AtomicInteger j = new AtomicInteger();

    public static class AddThread implements Runnable {
        public void run() {
            for (int k = 0; k < 10000; k++)
                i.incrementAndGet();
        }

    }


    static ConcurrentHashMap lockMap = new ConcurrentHashMap();

    static void lockKey() {
        String key = "UUID.randomUUID().toString()";
        if (lockMap.putIfAbsent(key, key) == null) {
            System.out.println("进来了");
            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lockMap.remove(key);
            j.incrementAndGet();
        } else {
            i.incrementAndGet();
            System.out.println("error");
        }
    }

    public static class lockKeyThread implements Runnable {
        public void run() {
            lockKey();
        }

    }

    static RateLimiter rateLimiter = RateLimiter.create(5.0);

    static void access() {
        if (rateLimiter.tryAcquire()) {
            System.out.println("进来了");
//            try {
//                sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            j.incrementAndGet();
        } else {
            i.incrementAndGet();
            System.out.println("error");
        }
    }

    public static class accessThread implements Runnable {
        int k;
        accessThread(int k) {
            this.k = k;
        }
        public void run() {
            System.out.println(Thread.currentThread().getName() + " hms " + k);
            access();
        }

    }

}
