package com.hms.test.testdemo;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import com.hms.test.testdemo.mapstruct.Car;
import com.hms.test.testdemo.mapstruct.CarDto;
import com.hms.test.testdemo.mapstruct.CarType;
import com.hms.test.testdemo.mapstruct.mapper.CarMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemoApplicationTests {

    private final Logger logger = LoggerFactory.getLogger(TestDemoApplicationTests.class);

    @Test
    public void contextLoads() {
    }

    /**
     * 布隆过滤
     */
    @Test
    public void bloomFilterTest() {
        long star = System.currentTimeMillis();
        BloomFilter<Integer> filter = BloomFilter.create(
                Funnels.integerFunnel(),
                10000000,
                0.01);
        for (int i = 0; i < 10000000; i++) {
            filter.put(i);
        }
        Assert.assertTrue(filter.mightContain(1));
        Assert.assertTrue(filter.mightContain(2));
        Assert.assertTrue(filter.mightContain(3));
        Assert.assertFalse(filter.mightContain(10000000));
        long end = System.currentTimeMillis();
        System.out.println("执行时间：" + (end - star));
    }

    /**
     * mapstruct bean转换 实体转换
     */
    @Test
    public void shouldMapCarToDto() {
        //given
        Car car = new Car("Morris", 5, CarType.SEDAN);

        //when
        CarDto carDto = CarMapper.INSTANCE.carToCarDto(car);

        //then
        assertThat(carDto).isNotNull();
        assertThat(carDto.getMake()).isEqualTo("Morris");
        assertThat(carDto.getSeatCount()).isEqualTo(5);
        assertThat(carDto.getType()).isEqualTo("SEDAN");
    }
}
