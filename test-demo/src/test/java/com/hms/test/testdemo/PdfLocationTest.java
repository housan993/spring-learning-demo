package com.hms.test.testdemo;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PdfLocationTest {

    private String templatePath = "TestEnglish.pdf";

    public static void main(String[] args) {
        String name = "asd";
        String fileName = name.substring(name.lastIndexOf(".") + 1) + ".class";
        System.out.println(fileName);
    }

    @Test
    public void FileTest() {

        try {
            InputStream stream = Class.forName("com.hms.test.testdemo.PdfLocationTest")
                    .getClassLoader().getResourceAsStream(templatePath);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        InputStream resourceAsStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(templatePath);
        URL url = Thread.currentThread()
                .getContextClassLoader().getResource(templatePath);

        System.out.println("URL is " + url.toString());
        System.out.println("protocol is "
                + url.getProtocol());
        System.out.println("authority is "
                + url.getAuthority());
        System.out.println("file name is " + url.getFile());
        System.out.println("host is " + url.getHost());
        System.out.println("path is " + url.getPath());
        System.out.println("port is " + url.getPort());
        System.out.println("default port is "
                + url.getDefaultPort());
        System.out.println("query is " + url.getQuery());
        System.out.println("ref is " + url.getRef());
    }


    @Test
    public void LocationTest() throws IOException, DocumentException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        InputStream in = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(templatePath);
        PdfReader pdfReader = new PdfReader(in);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);
        PdfContentByte overContent = pdfStamper.getOverContent(1);
        BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
//        BaseFont baseFont = BaseFont.createFont("STSongStd-Light", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        overContent.beginText();
        overContent.setFontAndSize(baseFont, 5);
        overContent.setTextMatrix(200, 200);

        for (int x = 0; x <= 1000; x += 2) {
            if (x % 10 == 0 && x != 0) {
                overContent.setFontAndSize(baseFont, 6);
                overContent.showTextAligned(Element.ALIGN_CENTER, x + "", x, 7, 0);
                overContent.showTextAligned(Element.ALIGN_CENTER, x + "", 7, x, 0);
                overContent.showTextAligned(Element.ALIGN_CENTER, "|", x, 0, 0);
                overContent.showTextAligned(Element.ALIGN_CENTER, "--", 0, x, 0);
            } else {
                overContent.setFontAndSize(baseFont, 4);
                overContent.showTextAligned(Element.ALIGN_CENTER, "|", x, 0, 0);
                overContent.showTextAligned(Element.ALIGN_CENTER, "--", 0, x, 0);
            }

            if (x % 100 == 0) {
                for (int y = 0; y <= 1000; y += 2) {

                    if (y % 100 == 0 && y != 0) {
                        overContent.setFontAndSize(baseFont, 8);
                        overContent.showTextAligned(Element.ALIGN_CENTER, x + "|" + y, x + 7, y + 7, 0);
                    }

                    overContent.setFontAndSize(baseFont, 5);
                    overContent.showTextAligned(Element.ALIGN_CENTER, "|", x, y, 0);
                    overContent.showTextAligned(Element.ALIGN_CENTER, "--", y, x, 0);
                }
            }
        }

        overContent.setFontAndSize(baseFont, 10);
        overContent.setTextMatrix(200, 200);
        overContent.showTextAligned(Element.ALIGN_CENTER, "yyyy年MM月dd日", 120, 222, 0);

        overContent.endText();
        pdfStamper.close();
        byte[] bytes = outputStream.toByteArray();

        File file = new File("location.pdf");
        String absolutePath = file.getAbsolutePath();
        System.out.print(absolutePath);

        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(outputStream.toByteArray());
        fos.close();
    }
}
