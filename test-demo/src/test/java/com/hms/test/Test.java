package com.hms.test;

import com.google.common.collect.Maps;

import java.util.HashMap;

/**
 * @author hms
 * @date 2019/1/10 10:11
 */
public class Test {
    public static void main(String[] args) {
        HashMap<Object, Object> map = Maps.newHashMap();
        String key = "hms";
        map.put("hms","hms");
        map.put(key,"1");
        System.out.println(map);
    }
}
