package com.hms.socket.socketchannelweb.service;

import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import com.hms.socket.socketchanelcommon.vo.criteria.P2pMsgCriteria;

/**
 * @author hms
 */
public interface RouteService {

    /**
     * 获取服务节点信息
     * @param userId
     * @return
     */
    SocketServerInfo distribution(Long userId);
}
