package com.hms.socket.socketchannelweb.service.impl;

import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;
import com.hms.socket.socketchanelcommon.utils.SessionSocketUtils;
import com.hms.socket.socketchanelcommon.vo.criteria.P2pMsgCriteria;
import com.hms.socket.socketchannelweb.service.RouteService;
import com.hms.socket.socketchannelweb.service.ServerCacheService;
import com.hms.utils.route.algorithm.RouteHandle;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author hms
 */
@Service
public class RouteServiceImpl implements RouteService {

    private final static Logger log = LoggerFactory.getLogger(RouteServiceImpl.class);

    @Resource
    private ServerCacheService serverCacheService;

    @Resource
    private RouteHandle routeHandle;

    @Override
    public SocketServerInfo distribution(Long userId) {
        log.info("获取所有服务器信息根据客户id[{}]分配,基于一致性hash算法", userId);
        String server = routeHandle.routeServer(serverCacheService.getAll(),
                String.valueOf(userId));
        return serverCacheService.analysisCache(server);
    }
}
