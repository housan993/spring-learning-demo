package com.hms.socket.socketchannelweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocketChannelWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocketChannelWebApplication.class, args);
    }

}
