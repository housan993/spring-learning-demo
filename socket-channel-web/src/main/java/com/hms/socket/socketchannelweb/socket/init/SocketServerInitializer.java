package com.hms.socket.socketchannelweb.socket.init;


import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import com.hms.socket.socketchannelweb.socket.handle.SocketServerHandle;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.timeout.IdleStateHandler;

/**
 *
 * @author hms
 */
public class SocketServerInitializer extends ChannelInitializer<Channel> {

    private final SocketServerHandle socketServerHandle = new SocketServerHandle() ;

    @Override
    protected void initChannel(Channel ch) throws Exception {

        ch.pipeline()
                //11 秒没有向客户端发送消息就发生心跳
                .addLast(new IdleStateHandler(11, 0, 0))
                // google Protobuf 编解码
                .addLast(new ProtobufVarint32FrameDecoder())
                .addLast(new ProtobufDecoder(SocketChanelRequestProto.SocketChanelRequest.getDefaultInstance()))
                .addLast(new ProtobufVarint32LengthFieldPrepender())
                .addLast(new ProtobufEncoder())
                .addLast(socketServerHandle);
    }
}
