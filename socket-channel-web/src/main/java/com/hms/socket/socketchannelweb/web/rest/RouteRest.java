package com.hms.socket.socketchannelweb.web.rest;

import com.hms.socket.socketchanelcommon.entity.SocketUsrInfo;
import com.hms.socket.socketchanelcommon.vo.criteria.GoogleProtocolCriteria;
import com.hms.socket.socketchanelcommon.vo.criteria.P2pMsgCriteria;
import com.hms.socket.socketchanelcommon.vo.criteria.SendMsgCriteria;
import com.hms.socket.socketchannelweb.service.RouteService;
import com.hms.socket.socketchannelweb.socket.client.SocketClient;
import com.hms.socket.socketchannelweb.socket.server.SocketServer;
import com.hms.utils.base.BaseController;
import com.hms.utils.base.BaseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author hms
 */
@RestController
@RequestMapping("route")
public class RouteRest extends BaseController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private SocketClient socketClient;

    @Resource
    private SocketServer socketServer;

    @Resource
    private RouteService routeService;

    /**
     * 向服务端发消息 Google ProtoBuf
     *
     * @param googleProtocolCriteria
     * @return
     */
    @PostMapping("login")
    public ResponseEntity<BaseView> login(@RequestBody GoogleProtocolCriteria googleProtocolCriteria) {
        try {
            log.info("入参={}", googleProtocolCriteria);
            socketClient.start(googleProtocolCriteria);
            BaseView baseView = new BaseView("登录成功");
            return new ResponseEntity<>(baseView, HttpStatus.OK);
        } catch (Exception e) {
            log.error("异常", e);
        }
        BaseView baseView = new BaseView("登录失败");
        return new ResponseEntity<>(baseView, HttpStatus.SEE_OTHER);
    }

    /**
     * 向服务端发消息 Google ProtoBuf
     *
     * @param googleProtocolCriteria
     * @return
     */
    @PostMapping("send-proto-buf-msg")
    public ResponseEntity<BaseView> sendProtoBufMsg(@RequestBody GoogleProtocolCriteria googleProtocolCriteria) {
        try {
            log.info("入参={}", googleProtocolCriteria);
            socketClient.sendGoogleProtocolMsg(googleProtocolCriteria);
            BaseView baseView = new BaseView("发送结束");
            return new ResponseEntity<>(baseView, HttpStatus.OK);
        } catch (Exception e) {
            log.error("异常", e);
        }
        BaseView baseView = new BaseView("发送失败");
        return new ResponseEntity<>(baseView, HttpStatus.SEE_OTHER);
    }

    /**
     * 向服务端发消息 Google ProtoBuf
     *
     * @param googleProtocolCriteria
     * @return
     */
    @PostMapping("log-out")
    public ResponseEntity<BaseView> logOut(@RequestBody GoogleProtocolCriteria googleProtocolCriteria) {
        try {
            log.info("入参={}", googleProtocolCriteria);
            SocketUsrInfo socketUsrInfo = new SocketUsrInfo();
            socketUsrInfo.setUserId(googleProtocolCriteria.getUserId());
            socketClient.close(socketUsrInfo);
            BaseView baseView = new BaseView("退出成功");
            return new ResponseEntity<>(baseView, HttpStatus.OK);
        } catch (Exception e) {
            log.error("异常", e);
        }
        BaseView baseView = new BaseView("退出失败");
        return new ResponseEntity<>(baseView, HttpStatus.SEE_OTHER);
    }

    /**
     * 服务端发消息
     *
     * @param sendMsgCriteria
     * @return
     */
    @PostMapping("send-msg")
    public ResponseEntity<BaseView> sendMsg(@RequestBody SendMsgCriteria sendMsgCriteria) {
        try {
            log.info("入参={}", sendMsgCriteria);
            socketServer.sendMsg(sendMsgCriteria);
            BaseView baseView = new BaseView("服务器发送成功");
            return new ResponseEntity<>(baseView, HttpStatus.OK);
        } catch (Exception e) {
            log.error("异常", e);
        }
        BaseView baseView = new BaseView("服务器发送失败");
        return new ResponseEntity<>(baseView, HttpStatus.SEE_OTHER);
    }

    /**
     * 私聊
     *
     * @param p2pMsgCriteria
     * @return
     */
    @PostMapping("p2p-msg")
    public ResponseEntity<BaseView> p2pMsg(@RequestBody P2pMsgCriteria p2pMsgCriteria) {
        try {
            log.info("入参={}", p2pMsgCriteria);
            SendMsgCriteria sendMsgCriteria = new SendMsgCriteria();
            sendMsgCriteria.setUserId(p2pMsgCriteria.getReceiveUserId());
            sendMsgCriteria.setMsg("用户[" + p2pMsgCriteria.getUserId() + "]对你说:" + p2pMsgCriteria.getMsg());
            socketServer.sendMsg(sendMsgCriteria);
            BaseView baseView = new BaseView("私聊成功");
            return new ResponseEntity<>(baseView, HttpStatus.OK);
        } catch (Exception e) {
            log.error("异常", e);
        }
        BaseView baseView = new BaseView("私聊失败");
        return new ResponseEntity<>(baseView, HttpStatus.SEE_OTHER);
    }
}
