package com.hms.socket.socketchannelweb.service;

import com.hms.socket.socketchanelcommon.entity.SocketServerInfo;

import java.util.List;

/**
 * @author hms
 */
public interface ServerCacheService {
    /**
     * 添加服务端信息
     *
     * @param key serverIp:nettyPort:serverPort
     */
    void addCache(String key);

    /**
     * 添加服务器信息详情
     *
     * @param serverIp
     * @param serverNettyPort
     * @param serverPort
     */
    void addCache(String serverIp, int serverNettyPort, int serverPort);

    /**
     * 先删除再更新 serverIp:serverNettyPort:serverPort
     *
     * @param currentInfos
     */
    void updateCache(List<String> currentInfos);

    /**
     * 获取所有服务节点
     *
     * @return
     */
    List<String> getAll();

    /**
     * 解析服务器节点信息
     *
     * @param key
     * @return
     */
    SocketServerInfo analysisCache(String key);
}
