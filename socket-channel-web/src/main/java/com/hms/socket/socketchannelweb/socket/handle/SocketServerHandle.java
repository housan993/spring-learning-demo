package com.hms.socket.socketchannelweb.socket.handle;

import com.hms.socket.socketchanelcommon.common.protocol.SocketChanelRequestProto;
import com.hms.socket.socketchanelcommon.constant.Constants;
import com.hms.socket.socketchanelcommon.entity.SocketUsrInfo;
import com.hms.socket.socketchanelcommon.utils.NettyUtils;
import com.hms.socket.socketchanelcommon.utils.SessionSocketUtils;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import okhttp3.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author hms
 */
@ChannelHandler.Sharable
public class SocketServerHandle extends SimpleChannelInboundHandler<SocketChanelRequestProto.SocketChanelRequest> {

    private final static Logger log = LoggerFactory.getLogger(SocketServerHandle.class);

    private final MediaType mediaType = MediaType.parse("application/json");

    /**
     * 取消绑定
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        //可能出现业务判断离线后再次触发 channelInactive
        SocketUsrInfo socketUsrInfo = SessionSocketUtils.getUserId((NioSocketChannel) ctx.channel());
        if (socketUsrInfo != null) {
            log.warn("[{}]触发 channelInactive 掉线!", socketUsrInfo.getUserName());
            userOffLine(socketUsrInfo, (NioSocketChannel) ctx.channel());
            ctx.channel().close();
        }
    }

    /**
     * 用户下线
     *
     * @param socketUsrInfo
     * @param channel
     * @throws IOException
     */
    private void userOffLine(SocketUsrInfo socketUsrInfo, NioSocketChannel channel) throws IOException {
        log.info("用户[{}]下线", socketUsrInfo.getUserName());
        SessionSocketUtils.remove(channel);
        SessionSocketUtils.removeSession(socketUsrInfo.getUserId());

        //清除路由关系
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            if (idleStateEvent.state() == IdleState.READER_IDLE) {

                log.info("定时检测客户端端是否存活");

//                HeartBeatHandler heartBeatHandler = SpringBeanFactory.getBean(ServerHeartBeatHandlerImpl.class);
//                heartBeatHandler.process(ctx);
            }
        }
        super.userEventTriggered(ctx, evt);
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx,
                                SocketChanelRequestProto.SocketChanelRequest msg) throws Exception {
        log.info("服务端收到msg={}", msg.getReqMsg());

        if (msg.getType() == Constants.CommandType.LOGIN) {
            //保存客户端与 Channel 之间的关系
            SessionSocketUtils.put(msg.getRequestId(), (NioSocketChannel) ctx.channel());
            SessionSocketUtils.saveSession(msg.getRequestId(), msg.getReqMsg());
            log.info("客户端[{}]上线成功", msg.getReqMsg());
        }

        //心跳更新时间
        if (msg.getType() == Constants.CommandType.PING) {
            NettyUtils.updateReaderTime(ctx.channel(), System.currentTimeMillis());
            //向客户端响应 pong 消息
            SocketChanelRequestProto.SocketChanelRequest heartBeat = SocketChanelRequestProto
                    .SocketChanelRequest.newBuilder()
                    .setRequestId(0L)
                    .setReqMsg("res")
                    .setType(Constants.CommandType.PING)
                    .build();
            ctx.writeAndFlush(heartBeat).addListeners((ChannelFutureListener) future -> {
                if (!future.isSuccess()) {
                    log.error("IO error,close Channel");
                    future.channel().close();
                }
            });
        }

    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {

        log.error(cause.getMessage(), cause);

    }

}
