package com.hms.redis.redislock.service.impl;


import com.google.common.base.Strings;
import com.hms.redis.redislock.config.RedisHelper;
import com.hms.redis.redislock.entity.LockDemo;
import com.hms.redis.redislock.mapper.LockDemoMapper;
import com.hms.redis.redislock.service.LockDemoService;
import com.hms.redis.redislock.web.criteria.GetLockRequest;
import com.hms.redis.redislock.web.view.GetLockResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.CRC32;

/**
 * @author hms
 * @date 2019/1/3 14:33
 */
@Service
public class LockDemoServiceImpl implements LockDemoService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private Environment environment;

    @Resource
    private LockDemoMapper lockDemoMapper;

    @Resource
    private RedisHelper redisHelper;

    private static AtomicInteger i = new AtomicInteger();

    @Cacheable(value = "cache-502")
    @Override
    public String redisValue(String key) {
        log.info("获取502缓存");
        Object value = redisHelper.getValue("502");
        log.info("获取502缓存放入cache-502");
        return value.toString();
    }

    @Cacheable(value = "cache-db", key = "#key")
    @Override
    public String dbValue(String key) {
        log.info("获取db缓存");
        LockDemo lockDemo = new LockDemo();
        lockDemo.setFindex(1);
        log.info("获取db缓存放入cache-db");
        return lockDemoMapper.select(lockDemo).toString();
    }

    @Override
    public boolean lockSetTime(String key, Integer expireTime) {
        boolean keyFlag = redisHelper.getDistributedLockSetTime(key, expireTime);
        if (keyFlag) {
            log.info("redis抢到锁了");
        } else {
            log.info("redis没抢到锁");
        }
        return keyFlag;
    }

    /**
     * 初始化记录,如果有记录update,如果没有记录insert
     */
    private LockDemo initLockRecord(String key) {
        // 查询记录是否存在
        LockDemo lockRecord = lockDemoMapper.selectByFlockName(key);
        if (null == lockRecord) {
            // 记录不存在，创建
            lockRecord = new LockDemo();
            lockRecord.setFlockName(key);
            lockRecord.setFcount(0);
            lockRecord.setFdesc("");
            lockRecord.setFdeadline(new Date());
            lockRecord.setFstatus((byte) 1);

            lockRecord.setFcreateTime(new Date());
            lockRecord.setFmodifyTime(new Date());
            lockDemoMapper.insert(lockRecord);
        }
        return lockRecord;
    }

    /**
     * 获取中央锁Key
     */
    private boolean getCenterLock(String key) {
        String prefix = "center_lock_";
        CRC32 crc32 = new CRC32();
        crc32.update(key.getBytes());
        crc32.getValue();

        Long hash = crc32.getValue();
        if (null == hash) {
            return false;
        }
        //取crc32中的最后两位值
        Integer len = hash.toString().length();
        String slot = hash.toString().substring(len - 2);

        String centerLockKey = prefix + slot;
        lockDemoMapper.selectForUpdate(centerLockKey);
        return true;
    }


    /**
     * 获取锁，代码片段
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public GetLockResponse getLock(GetLockRequest request) {
        // 检测参数
        if (StringUtils.isEmpty(request.getLockName())) {
            throw new RuntimeException("key不能为空");
        }

        // 兼容参数初始化
        request.setExpireTime(null == request.getExpireTime() ? 31536000 : request.getExpireTime());
        request.setDesc(Strings.isNullOrEmpty(request.getDesc()) ? "" : request.getDesc());
        long nowTime = System.currentTimeMillis();

        GetLockResponse response = new GetLockResponse();
        response.setLock(0);

        boolean b = lockSetTime(request.getLockName(), 30);
        if (b) {
            log.info("redis获取到锁了lockName={}", request.getLockName());
        } else {
            log.info("redis未获取到锁lockName={}", request.getLockName());
            return response;
        }

        // 获取中央锁，初始化记录
        getCenterLock(request.getLockName());
        LockDemo lockRecord = initLockRecord(request.getLockName());


        i.incrementAndGet();
        log.error("测试信息*************************************" + i);
        // 未释放锁或未过期，获取失败
        if (lockRecord.getFstatus() == (byte) 1
                && lockRecord.getFdeadline().getTime() > nowTime) {
            return response;
        }

        // 获取锁
        Date deadline = new Date(nowTime + request.getExpireTime() * 1000);

        LockDemo lockDemo = new LockDemo();
        lockDemo.setFindex(lockRecord.getFindex());
        lockDemo.setFlockName(request.getLockName());
        lockDemo.setFdeadline(deadline);
        lockDemo.setFcount(0);
        lockDemo.setFdesc(request.getDesc());
        lockDemo.setFstatus((byte) 1);

        lockDemo.setFmodifyTime(new Date());

        int num = lockDemoMapper.updateByPrimaryKeySelective(lockDemo);
        response.setLock(1);

        log.error("测试信息1*************************************");
        response.setNum(i.intValue());
        return response;
    }
}
