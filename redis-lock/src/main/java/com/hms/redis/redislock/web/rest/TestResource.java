package com.hms.redis.redislock.web.rest;


import com.google.common.collect.Maps;
import com.hms.redis.redislock.config.RedisHelper;
import com.hms.redis.redislock.service.LockDemoService;
import com.hms.redis.redislock.web.criteria.GetLockRequest;
import com.hms.redis.redislock.web.view.GetLockResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;

/**
 * @author hms
 * @date 2018/12/26 14:28
 */

@RestController
@RequestMapping("redis")
public class TestResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private LockDemoService lockDemoService;

    @Resource
    private RedisHelper redisHelper;

    @Resource
    private CacheManager cacheManager;

    @Resource
    private ExecutorService executorService;

    @PostMapping("/testUtilsExecutor")
    public ResponseEntity<Map<String, Object>> testUtilsExecutor() {
        HashMap<String, Object> map = Maps.newHashMap();

        int THREAD_COUNT = 1000;
        int CONCURRENT_COUNT = 200;

        try {
            Semaphore semaphore = new Semaphore(CONCURRENT_COUNT);
            CountDownLatch countDownLatch = new CountDownLatch(THREAD_COUNT);
            for (int i = 0; i < THREAD_COUNT; i++) {
                int finalI = 1;
                executorService.execute(() -> {
                    try {
                        semaphore.acquire();

                        GetLockRequest request = new GetLockRequest();
                        request.setLockName("hms" + finalI);
                        request.setDesc("hms-lock"+ finalI);
                        request.setExpireTime(30L);
                        GetLockResponse lock = lockDemoService.getLock(request);
                        log.info("lock={}", lock);

                        semaphore.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    countDownLatch.countDown();
                });
            }
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/testUtils")
    public ResponseEntity<Map<String, Object>> testUtils() {
        HashMap<String, Object> map = Maps.newHashMap();

//        boolean lockSetTime = lockDemoService.lockSetTime("hms", 30);
        GetLockRequest request = new GetLockRequest();
        request.setLockName("hms");
        request.setDesc("hms-lock");
        request.setExpireTime(30L);
        GetLockResponse lock = lockDemoService.getLock(request);
        log.info("lock={}", lock);


        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    /**
     * @return
     */
    @PostMapping("/test502")
    public ResponseEntity<Map<String, Object>> test502() {
        HashMap<String, Object> map = Maps.newHashMap();
        String value = lockDemoService.redisValue("cache502");
        log.info("value={}", value);

        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/testdb")
    public ResponseEntity<Map<String, Object>> testdb() {
        HashMap<String, Object> map = Maps.newHashMap();

        String dbValue = lockDemoService.dbValue("cachedb");
        log.info("dbValue={}", dbValue);

        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/cache502")
    public ResponseEntity<Map<String, Object>> cache502() {
        HashMap<String, Object> map = Maps.newHashMap();

        Object cacheKey = Objects.requireNonNull(cacheManager.getCache("cache-502")).get("cache502");
        log.info("cacheKey={}", cacheKey);
        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/cachedb")
    public ResponseEntity<Map<String, Object>> cachedb() {
        HashMap<String, Object> map = Maps.newHashMap();

        Object cacheDbKey = Objects.requireNonNull(cacheManager.getCache("cache-db")).get("cachedb");
        log.info("cacheDbKey={}", cacheDbKey);
        map.put("code", "000000");
        map.put("data", "infoList");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @RequestMapping("/uid")
    public String uid(HttpSession session) {
        UUID uid = (UUID) session.getAttribute("uid");
        if (uid == null) {
            uid = UUID.randomUUID();
        }
        session.setAttribute("uid", uid);
        return session.getId();
    }

}
