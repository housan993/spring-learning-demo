package com.hms.redis.redislock.entity;

import com.hms.redis.redislock.base.BaseEntity;

/**
 * @author hms
 * @date 2019/1/3 16:23
 */
public class Author extends BaseEntity {
    private String name;
    private String intro_l;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntro_l() {
        return intro_l;
    }

    public void setIntro_l(String intro_l) {
        this.intro_l = intro_l;
    }
}
