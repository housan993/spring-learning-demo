package com.hms.redis.redislock.entity;

import com.hms.redis.redislock.base.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "t_lock")
public class LockDemo extends BaseEntity {
    /**
     * 自增索引id
     */
    @Id
    @Column(name = "Findex")
    private Integer findex;

    /**
     * 锁名key值
     */
    @Column(name = "Flock_name")
    private String flockName;

    /**
     * 计数器
     */
    @Column(name = "Fcount")
    private Integer fcount;

    /**
     * 锁过期时间
     */
    @Column(name = "Fdeadline")
    private Date fdeadline;

    /**
     * 值/描述
     */
    @Column(name = "Fdesc")
    private String fdesc;

    /**
     * 创建时间
     */
    @Column(name = "Fcreate_time")
    private Date fcreateTime;

    /**
     * 修改时间
     */
    @Column(name = "Fmodify_time")
    private Date fmodifyTime;

    /**
     * 记录状态，0：无效，1：有效
     */
    @Column(name = "Fstatus")
    private Byte fstatus;

    /**
     * 获取自增索引id
     *
     * @return Findex - 自增索引id
     */
    public Integer getFindex() {
        return findex;
    }

    /**
     * 设置自增索引id
     *
     * @param findex 自增索引id
     */
    public void setFindex(Integer findex) {
        this.findex = findex;
    }

    /**
     * 获取锁名key值
     *
     * @return Flock_name - 锁名key值
     */
    public String getFlockName() {
        return flockName;
    }

    /**
     * 设置锁名key值
     *
     * @param flockName 锁名key值
     */
    public void setFlockName(String flockName) {
        this.flockName = flockName;
    }

    /**
     * 获取计数器
     *
     * @return Fcount - 计数器
     */
    public Integer getFcount() {
        return fcount;
    }

    /**
     * 设置计数器
     *
     * @param fcount 计数器
     */
    public void setFcount(Integer fcount) {
        this.fcount = fcount;
    }

    /**
     * 获取锁过期时间
     *
     * @return Fdeadline - 锁过期时间
     */
    public Date getFdeadline() {
        return fdeadline;
    }

    /**
     * 设置锁过期时间
     *
     * @param fdeadline 锁过期时间
     */
    public void setFdeadline(Date fdeadline) {
        this.fdeadline = fdeadline;
    }

    /**
     * 获取值/描述
     *
     * @return Fdesc - 值/描述
     */
    public String getFdesc() {
        return fdesc;
    }

    /**
     * 设置值/描述
     *
     * @param fdesc 值/描述
     */
    public void setFdesc(String fdesc) {
        this.fdesc = fdesc;
    }

    /**
     * 获取创建时间
     *
     * @return Fcreate_time - 创建时间
     */
    public Date getFcreateTime() {
        return fcreateTime;
    }

    /**
     * 设置创建时间
     *
     * @param fcreateTime 创建时间
     */
    public void setFcreateTime(Date fcreateTime) {
        this.fcreateTime = fcreateTime;
    }

    /**
     * 获取修改时间
     *
     * @return Fmodify_time - 修改时间
     */
    public Date getFmodifyTime() {
        return fmodifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param fmodifyTime 修改时间
     */
    public void setFmodifyTime(Date fmodifyTime) {
        this.fmodifyTime = fmodifyTime;
    }

    /**
     * 获取记录状态，0：无效，1：有效
     *
     * @return Fstatus - 记录状态，0：无效，1：有效
     */
    public Byte getFstatus() {
        return fstatus;
    }

    /**
     * 设置记录状态，0：无效，1：有效
     *
     * @param fstatus 记录状态，0：无效，1：有效
     */
    public void setFstatus(Byte fstatus) {
        this.fstatus = fstatus;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}