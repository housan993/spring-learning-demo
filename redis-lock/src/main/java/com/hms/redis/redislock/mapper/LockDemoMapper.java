package com.hms.redis.redislock.mapper;

import com.hms.redis.redislock.entity.LockDemo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

public interface LockDemoMapper extends Mapper<LockDemo> {

	/*CUSTOM_CODE_START*/

    /**
     * 中央锁
     * @param flockName
     * @return
     */
    @Select("SELECT * from t_lock WHERE Flock_name = #{flockName} for update ")
    LockDemo selectForUpdate(@Param("flockName") String flockName);


    @Select("SELECT * from t_lock WHERE Flock_name = #{flockName} ")
    LockDemo selectByFlockName(@Param("flockName") String flockName);
	/*CUSTOM_CODE_END*/
}