package com.hms.redis.redislock.service;


import com.hms.redis.redislock.web.criteria.GetLockRequest;
import com.hms.redis.redislock.web.view.GetLockResponse;

/**
 *
 *
 * @author hms
 * @date 2019/1/3 14:32
 */
public interface LockDemoService {

    String redisValue(String key);

    String dbValue(String key);

    boolean lockSetTime(String key, Integer expireTime);


    GetLockResponse getLock(GetLockRequest request);
}
