package com.hms.utils.utils;

import com.alibaba.fastjson.JSONObject;
import com.hms.utils.constant.Constants;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author hms
 */
public class OkHttpUtils {

    private final static Logger log = LoggerFactory.getLogger(OkHttpUtils.class);


    private static MediaType mediaType = MediaType.parse("application/json");

    public static String post(String url, JSONObject jsonObject, OkHttpClient okHttpClient) {
        log.info("post请求json信息={}", jsonObject);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), mediaType);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        return newCall(request, okHttpClient);
    }

    public static String get(HttpUrl.Builder urlBuilder, OkHttpClient okHttpClient) {
        log.info("get请求json信息={}", urlBuilder.toString());
        Request request = new Request.Builder()
                .url(urlBuilder.build())
                .get()
                .build();

        return newCall(request, okHttpClient);
    }

    private static String newCall(Request request, OkHttpClient okHttpClient) {
        try {
            log.info("请求信息={}", request);
            Response response = okHttpClient.newCall(request).execute();
            log.info("返回信息={}", response);

            if (!response.isSuccessful()) {
                response.close();
                throw new IOException("Unexpected code " + response);
            }

            try (ResponseBody body = response.body()) {
                assert body != null;
                String responseBody = body.string();
                log.info("返回body信息={}", responseBody);
                return responseBody;
            }
        } catch (IOException e) {
            log.error("请求异常", e);
        }
        return Constants.JSON_RESULT_DEFAULT;
    }
}
