package com.hms.utils.base;

/**
 * @author hms
 * @date 2019/2/2 15:17
 */
public enum BaseEnum {
    /**
     * 系统枚举
     */
    SUCCESS("000000", "成功"),
    ERROR("111111", "系统错误")
    ;

    private String code;
    private String msg;

    BaseEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }}
