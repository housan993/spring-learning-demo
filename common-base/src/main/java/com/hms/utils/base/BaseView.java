package com.hms.utils.base;


/**
 * 合并参数
 *
 * @author hms
 * @date 2018/11/6 16:27
 */
public class BaseView extends BaseEntity {
    private String code = "000000";
    private String msg;
    private Object trackNo;
    private Object data;

    public BaseView() {
    }

    public BaseView(String msg) {
        this.msg = msg;
    }

    public BaseView(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseView(String msg, Object data) {
        this.msg = msg;
        this.data = data;
    }

    public BaseView(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public BaseView(BaseEnum baseEnum) {
        this.code = baseEnum.getCode();
        this.msg = baseEnum.getMsg();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getTrackNo() {
        return trackNo;
    }

    public void setTrackNo(Object trackNo) {
        this.trackNo = trackNo;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
