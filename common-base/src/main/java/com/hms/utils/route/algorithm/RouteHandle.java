package com.hms.utils.route.algorithm;

import java.util.List;

/**
 * @author hms
 */
public interface RouteHandle {

    /**
     * 再一批服务器里进行路由
     *
     * @param values
     * @param key
     * @return
     */
    String routeServer(List<String> values, String key);
}
