package com.hms.utils.route.algorithm.consistenthash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * hash一致性算法
 *
 * @author hms
 */
public abstract class AbstractConsistentHash {

    /**
     * 新增节点
     *
     * @param key
     * @param value
     */
    protected abstract void add(long key, String value);

    /**
     * 排序节点，数据结构自身支持排序可以不用重写
     */
    protected void sort() {
    }

    /**
     * 根据当前的 key 通过一致性 hash 算法的规则取出一个节点
     *
     * @param value
     * @return
     */
    protected abstract String getFirstNodeValue(String value);

    /**
     * 传入节点列表以及客户端信息获取一个服务节点
     *
     * @param values
     * @param key
     * @return
     */
    public String process(List<String> values, String key) {

        for (String value : values) {
            add(hash(value), value);
        }
        sort();

        return getFirstNodeValue(key);
    }

    /**
     * copy-from xxl_job ExecutorRouteConsistentHash.java
     * get hash code on 2^32 ring (md5散列的方式计算hash值)
     *
     * @param key
     * @return
     */
    public long hash(String key) {

        // md5 byte
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 not supported", e);
        }
        md5.reset();
        byte[] keyBytes = key.getBytes(StandardCharsets.UTF_8);

        md5.update(keyBytes);
        byte[] digest = md5.digest();

        // hash code, Truncate to 32-bits
        long hashCode = ((long) (digest[3] & 0xFF) << 24)
                | ((long) (digest[2] & 0xFF) << 16)
                | ((long) (digest[1] & 0xFF) << 8)
                | (digest[0] & 0xFF);

        return hashCode & 0xffffffffL;
    }
}
